package com.aeros.steps;

import com.aeros.model.User;
import com.aeros.pages.CommonPage;
import com.aeros.pages.ProjectsDashboardPage;
import com.aeros.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static com.aeros.constants.Const.Url.AUTO_AFL_DEFAULT_URL;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UserSteps extends ScenarioSteps {

    private LoginPage loginPage;
    private CommonPage commonPage;
    private ProjectsDashboardPage dashboardPage;
    
   
    @Step("Authorizes into application")
    public void logsInToAflWeb(User user) {
        authorizeIntoApplication(user);
        reAuthorizeIntoApp(user);
        verifyUserNameLabel(user.getUserEmail());
    }

    @Step
    public void openWebBasePage() {
        loginPage.open();
    }

    private void authorizeIntoApplication(User user) {
        enterUserCredentials(user);
        signIntoApp();
    }

    public void signIntoApp() {
        loginPage.signIntoApp();
    }

    public void reAuthorizeIntoApp(User user) {
        loginPage.reAuthorizeIntoApp(user);
    }



    private void enterUserCredentials(User user) {
        enterUserName(user.getUserEmail());
        enterPassword(user.getUserPassword());
    }

    public void enterUserName(String userName) {
        loginPage.enterUsername(userName);
    }

    public void enterPassword(String password) {
        loginPage.enterPassword(password);
    }

    @Step
    
    public void verifyUserNameLabel(String userName) {
           assertThat(getUserNameLabel(), is(equalTo("Bhakti Ahire")));
    }

   private String getUserNameLabel() {
        dashboardPage.waitForTextToAppear(dashboardPage.getHeaderTab().getNavigationTab().getUserNameLabel(), 10);
        return dashboardPage.getHeaderTab().getNavigationTab().getUserNameLabel();
    }

    @Step
    public void verifyHeaderPageLabel(String label) {
        assertThat(dashboardPage.getHeaderTab().getHeaderPageLabel(), is(equalTo(label)));
    }

    @Step
    public void logOutFromApp() {
        dashboardPage.getHeaderTab().getNavigationTab().openUserNameDropDown();
        dashboardPage.getHeaderTab().getNavigationTab().clickOnLogoutLink();
        loginPage.waitForAuthorizationFormVisibility();
        verifyDefaultPageUrl();
    }

    public void verifyDefaultPageUrl(){
        assertThat(loginPage.getCurrentPageUrl(), is(equalTo(AUTO_AFL_DEFAULT_URL)));
    }

}
