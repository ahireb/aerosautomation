package com.aeros.steps;

import com.aeros.model.UIReportFileByBhakti;

import net.thucydides.core.steps.ScenarioSteps;

public class UIReportstepsByBhakti extends ScenarioSteps {
	
	private UIReportFileByBhakti uireportfile;
	public void openUIFileToRead(String file)
	{
		uireportfile = new UIReportFileByBhakti(file);
	}
	
	public void UI_Steps()
	{
		String line;
		String title;
		String info;
		String result;
		String result_failuare;
		
		while(true)
		{
			line = uireportfile.getNextLine();
			if ( line == null )
				break;
			title = uireportfile.getStepName(line);
			if (title == null)
			{
				System.out.println("Invalid UI report file\n" );
				return;
			}	
			
			line = uireportfile.getNextLine();
			if( line == null )
				break;
			info = uireportfile.getStepInfo(line);
			if ( info == null )
			{
				System.out.println("Invalid UI report file\n");
				return;
			}
			
			line = uireportfile.getNextLine();
			if ( line == null )
				break;
			result = uireportfile.getStepResult(line);
			if ( result == null )
			{
				System.out.println("Invalid UI report file\n");
				return;
				
			}
			
			line = uireportfile.getNextLine();
			if ( line == null)
				break;
			result_failuare = uireportfile.getStepFailure(line);
			if ( result_failuare == null )
			{
				System.out.println("Invalid UI report file\n");
				return;
			}
				
			
		}
		
		
	}
	
	

}
