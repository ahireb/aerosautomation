package com.aeros.steps;

import com.aeros.constants.Const;
import com.aeros.model.FiberGroup;
import com.aeros.pages.CommonPage;
import com.aeros.pages.FiberGroupPage;
import com.aeros.utils.PatternMatcherUtils;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.List;
import java.util.Set;

import static com.aeros.constants.Const.Actions.ADD_ACTION;
import static com.aeros.constants.Const.Actions.UPLOAD_ACTION;
import static com.aeros.constants.Const.RegExp.TEXT_AFTER_COLON_EXPRESSION;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.core.Is.is;

public class FiberGroupSteps extends ScenarioSteps {
    private FiberGroupPage fiberGroupPage;
    private CommonPage commonPage;
    private FiberGroup fiberGroup;

    @Step
    public void verifyHeaderPageLabel(String label) {
        assertThat(fiberGroupPage.getHeaderTab().getProjectInfoTitle(), is(equalTo(label)));
    }

    public void waitForPageTitleDisappear(String label) {
        fiberGroupPage.getHeaderTab().waitUntilTextToDisappear(label);
    }

    @Step
    public void verifyProjectNavBarOptionIsSelected(String label) {
        assertThat(fiberGroupPage.getProjectsNavBarComponent().isProjectNavBarOptionSelected(label), is(true));
    }

    @Step
    public void verifyPanelHeadingsPresence(String mediaTypeLabel, String testSetupsLabel) {
        assertThat(fiberGroupPage.getPanelHeadingLabels(), hasItems(mediaTypeLabel, testSetupsLabel));
    }

    public void selectFiberGroupAction(String option) {
    	
        fiberGroupPage.getFiberGroupActionsPanel().selectFiberGroupAction(option);
        fiberGroupPage.getModalWindowPage().waitForModalWindowVisibility();
    }

    @StepGroup
    public void createAFiberGroup(String fiberGroupName) {
        selectFiberGroupAction(ADD_ACTION);
        enterFiberGroupName(fiberGroupName);
        clickOnCreateFiberGroupButton();
    }

    @StepGroup
    public void createFiberGroupCollection(FiberGroup fiberGroupName, String collectionName) {
        selectFiberGroupByName(fiberGroupName.getFiberGroupName());
        selectFiberGroupAction(fiberGroupName.getFiberGroupName(), UPLOAD_ACTION);
        enterCollectionName(collectionName);
        clickOnAddCollectionSaveButton();
    }
    @Step
    public void waitForFiberGroupToBePresent(FiberGroup fiberGroup) {
        fiberGroupPage.waitForFiberGroupToBePresent(fiberGroup);
    }

    @Step
    public void enterFiberGroupName(String fiberGroupName) {
        fiberGroupPage.getFiberGroupModalWindow().enterFiberGroupName(fiberGroupName);
    }

    public void clickOnCreateFiberGroupButton() {
        fiberGroupPage.getFiberGroupModalWindow().clickOnCreateFiberGroupButton();
    }
    
    public void setFiberCount(int num)
    {
    	fiberGroupPage.getFiberGroupModalWindow().setFiberCount(num);
    }

    @Step
    public void isCreateFiberGroupButtonActive(boolean state) {
        assertThat(fiberGroupPage.getFiberGroupModalWindow().isCreateFiberGroupButtonActive(), is(state));
    }

    @Step
    public void selectFiberTypeOption(String option) {
        fiberGroupPage.getFiberGroupModalWindow().selectFiberTypeOptionByValue(option);
    }

    @Step
    public void isFiberGroupPresent(FiberGroup fiberGroup) {
        waitForFiberGroupToBePresent(fiberGroup);
        assertThat(fiberGroupPage.isFiberGroupPresent(fiberGroup), is(true));
    }

    @Step
    public void isFiberGroupAbsent(FiberGroup fiberGroup) {
        waitForFiberGroupToBeAbsent(fiberGroup);
        assertThat(fiberGroupPage.isFiberGroupPresent(fiberGroup), is(false));
    }

    public void waitForElementInvisibility(FiberGroup fiberGroup){
        fiberGroupPage.waitForElementAbscence(fiberGroupPage.getFiberGroupBy(fiberGroup));
    }

    public void waitForFiberGroupToBeAbsent(FiberGroup fiberGroup) {
        fiberGroupPage.waitForFiberGroupToBeAbsent(fiberGroup);
    }

    @Step
    public void isAppliedProjectCollectionButtonPresent(boolean state) {
        assertThat(fiberGroupPage.isAppliedProjectCollectionButtonPresent(), is(state));
    }

    public void selectFiberGroupAction(String fiberGroupName, String action) {
        fiberGroupPage.selectFiberGroupAction(fiberGroupName, action);
        fiberGroupPage.getModalWindowPage().waitForModalWindowVisibility();
    }

    @Step
    public void isFiberGroupSizeCorrect(int fiberGroupNumber) {
        assertThat(fiberGroupPage.getFiberGroupListSize(), is(equalTo(fiberGroupNumber)));
    }

    public Set<String> getSelectedFiberGroupNames() {
        return fiberGroupPage.getSelectedFiberGroupNames();
    }

    public void selectMediaTypeByName(String mediaType) {
        fiberGroupPage.selectMediaTypeByName(mediaType);
    }

    @Step
    public void clickOnProjectUploadButton() {
        fiberGroupPage.clickOnProjectUploadButton();
    }

    @StepGroup
    public void createProjectCollection(String projectCollectionName) {
        clickOnProjectUploadButton();
        enterCollectionName(projectCollectionName);
        clickOnAddCollectionSaveButton();
    }

    @Step
    public void isNumberOfProjectAttachedDocumentsCorrect(int documentsNumber) {
        assertThat(fiberGroupPage.getNumberOfAttachedDocuments(), is(equalTo(documentsNumber)));
    }

    @Step
    public void isNumberOfFiberGroupAttachedDocumentsCorrect(FiberGroup fiberGroup, int documentsNumber) {
        assertThat(fiberGroupPage.getNumberOfFiberGroupAttachedDocuments(fiberGroup), is(equalTo(documentsNumber)));
    }

    public void clickOnFiberGroupCollection(FiberGroup fiberGroup) {
        fiberGroupPage.clickOnFiberGroupCollection(fiberGroup);
    }

    @Step
    public void isFiberGroupCollectionNameCorrect(FiberGroup fiberGroup, String fiberGroupCollectionName) {
        assertThat(fiberGroupPage.getFiberGroupCollectionNameText(fiberGroup), is(equalTo(fiberGroupCollectionName)));
    }

    public void clickOnDeleteFiberGroupCollectionButton(FiberGroup fiberGroup) {
        fiberGroupPage.clickOnDeleteFiberGroupCollectionButton(fiberGroup);
    }

    @Step
    public void clickOnRemoveProjectCollectionButton() {
        fiberGroupPage.clickOnRemoveProjectCollectionButton();
    }

    @Step
    public void clickOnProjectCollectionButton() {
        fiberGroupPage.clickOnProjectCollectionButton();
    }

    @Step
    public void isProjectCollectionNameCorrect(String collectionName) {
        assertThat(fiberGroupPage.getProjectCollectionNameText(), is(equalTo(collectionName)));
    }

    @Step
    public void clickOnTestSetupButton() {
        fiberGroupPage.clickOnTestSetupButton();
    }

    public List<String> getMediaTypeNames() {
        return fiberGroupPage.getMediaTypeNames();
    }

    @StepGroup
    public void verifyCreatedMediaTypes(List<String> mediaTypeList) {
        for (String mediaType : mediaTypeList) {
            fiberGroup = FiberGroup.builder(String.format(Const.DefaultValues.FIBER_GROUP_MEDIA_TYPE_NAME, mediaType)).setFiberType(mediaType).build();
            selectMediaTypeByName(fiberGroup.getFiberType());
            enterFiberGroupName(fiberGroup.getFiberGroupName());
            assertThat(fiberGroup.getFiberType(), is(equalTo(getFiberTypeValue())));
            clickOnCreateFiberGroupButton();
            waitForFiberGroupToBePresent(fiberGroup);
            isFiberGroupPresent(fiberGroup);
        }
    }

    @Step
    public void isModalContentBlockDisplayed() {
        assertThat(fiberGroupPage.getModalWindowPage().isModalContentBlockDisplayed(), is(true));
    }

    @Step
    public void isModalWindowTitleCorrect(String text) {
        fiberGroupPage.getModalWindowPage().waitForModalWindowVisibility();
        assertThat(fiberGroupPage.getModalWindowPage().getModalWindowTitle(), is(equalTo(text)));
    }

    @Step
    public void isFormattedWindowTitleCorrect(String title, String text) {
        assertThat(fiberGroupPage.getModalWindowPage().getModalWindowTitle(), is(equalTo(String.format(title, text))));
    }

    @Step
    public void isProjectAttachmentLabelCorrect(String title, String text) {
        assertThat(fiberGroupPage.getProjectCollectionWindow().getProjectAttachmentText(), is(equalTo(String.format(title, text))));
    }

    @Step
    public void closeCollectionWindow() {
        fiberGroupPage.getProjectCollectionWindow().closeProjectCollectionWindow();
    }

    @Step
    public void isCloseButtonEnabled() {
        assertThat(fiberGroupPage.getProjectCollectionWindow().isCloseButtonEnabled(), is(true));
    }

    @Step
    public void editFiberGroupName(String fiberGroupName) {
        fiberGroupPage.getEditFiberGroupWindow().enterFiberGroupName(fiberGroupName);
    }

    @Step
    public void clickOnSubmitButton() {
        fiberGroupPage.getEditFiberGroupWindow().clickOnSubmitButton();
    }

    @Step
    public void selectFiberGroupByName(String fiberGroupName) {
        fiberGroupPage.selectFiberGroupByName(fiberGroupName);
    }

    @Step
    public void verifyFiberGroupIsSelected(String fiberGroupName, boolean state) {
        assertThat(fiberGroupPage.isFiberGroupSelected(fiberGroupName), is(state));
    }

    @Step
    public void isFiberGroupCollectionButtonPresent(FiberGroup fiberGroup, boolean state) {
        assertThat(fiberGroupPage.isFiberGroupCollectionButtonPresent(fiberGroup), is(state));
    }


    @Step
    public void verifyFiberGroupActionIsEnabled(String fiberGroupName, boolean state) {
        assertThat(fiberGroupPage.getFiberGroupActionsPanel().isFiberGroupActionEnabled(fiberGroupName), is(state));
    }

    @Step
    public void verifyDeletingFiberGroupNamesCorrect(Set<String> fiberGroupNameSet) {
        assertThat(fiberGroupPage.getModalWindowPage().getModalWindowSetOptions(), is(equalTo(fiberGroupNameSet)));
    }

    public void waitForAppliedProjectCollectionButtonAbscence() {
        fiberGroupPage.waitForAppliedProjectCollectionButtonAbsence();
    }

    public void clickOnModalWindowButtonByText(String text) {
        fiberGroupPage.getModalWindowPage().clickOnModalWindowButtonByText(text);
    }

    private String getFiberGroupNameValue() {
        return fiberGroupPage.getFiberGroupModalWindow().getFiberGroupNameValue();
    }


    public String getFiberTypeValue() {
        return fiberGroupPage.getFiberGroupModalWindow().getFiberTypeValue();
    }

    private String getStartingFiberNumber() {
        return fiberGroupPage.getFiberGroupModalWindow().getStartingFiberNumber();
    }

    public void isStartingFiberIsEqualTo(String value){
        assertThat(getStartingFiberNumber(), is(equalTo(value)));
    }

    private String getNumberOfFibers() {
        return fiberGroupPage.getFiberGroupModalWindow().getNumberOfFibers();
    }

    public void isNumberOfFibersIsEqualTo(String value){
        assertThat(getNumberOfFibers(), is(equalTo(value)));
    }

    private String getNumberOfConnectionsValue() {
        return fiberGroupPage.getFiberGroupModalWindow().getConnectionsCount();
    }

    private String getNumberOfSplicesValue() {
        return fiberGroupPage.getFiberGroupModalWindow().getSplicesCount();
    }

    private String getEnd1Name() {
        return fiberGroupPage.getFiberGroupModalWindow().getEnd1ConnectorName();
    }

    private String getEnd2Name() {
        return fiberGroupPage.getFiberGroupModalWindow().getEnd2ConnectorName();
    }

    private String getEndConnectorValueByName(String name) {
        return PatternMatcherUtils.getMatcherString(fiberGroupPage.getFiberGroupModalWindow().getEndConnectorValueByName(name),
                TEXT_AFTER_COLON_EXPRESSION, 0);
    }

    private String getEndConnectorSelectedValue(String connector) {
        return fiberGroupPage.getFiberGroupModalWindow().getEndConnectorSelectedValue(connector);
    }


    private String getComment() {
        return fiberGroupPage.getFiberGroupModalWindow().getFiberGroupComment();
    }


    @Step
    public FiberGroup getDefaultFiberGroupDataObject() {
        FiberGroup newFiberGroupObject = FiberGroup.builder(getFiberGroupNameValue()).
                setFiberType(getFiberTypeValue()).
                setStartingFiberNumber(getStartingFiberNumber()).
                setNumberOfFibers(getNumberOfFibers()).
                setNumberOfConnections(getNumberOfConnectionsValue()).
                setNumberOfSplices(getNumberOfSplicesValue()).
                setEnd1Name(getEnd1Name()).
                setEnd1Connector(getEndConnectorValueByName("End1 Connector")).
                setEnd2Name(getEnd2Name()).
                setEnd2Connector(getEndConnectorValueByName("End2 Connector")).
                build();
        return newFiberGroupObject;
    }

    @Step
    public FiberGroup getFiberGroupDataObject() {
        FiberGroup newFiberGroupObject = FiberGroup.builder(getFiberGroupNameValue()).
                setComment(getComment()).
                setFiberType(getFiberTypeValue()).
                setStartingFiberNumber(getStartingFiberNumber()).
                setNumberOfFibers(getNumberOfFibers()).
                setNumberOfConnections(getNumberOfConnectionsValue()).
                setNumberOfSplices(getNumberOfSplicesValue()).
                setEnd1Name(getEnd1Name()).
                setEnd1Connector(getEndConnectorValueByName("End1 Connector")).
                setEnd1ConnectorOption(getEndConnectorSelectedValue("End1 Connector")).
                setEnd2Name(getEnd2Name()).
                setEnd2Connector(getEndConnectorValueByName("End2 Connector")).
                setEnd2ConnectorOption(getEndConnectorSelectedValue("End2 Connector")).
                build();
        return newFiberGroupObject;
    }


    public FiberGroup getViewInfoDefaultFiberGroupData() {
        FiberGroup viewInfoFiberGroupData = FiberGroup.builder(fiberGroupPage.getModalWindowPage().getInfoValue("Name")).
                setFiberType(fiberGroupPage.getModalWindowPage().getInfoValue("Fiber Type")).
                setStartingFiberNumber(fiberGroupPage.getModalWindowPage().getInfoValue("Starting Fiber")).
                setNumberOfFibers(fiberGroupPage.getModalWindowPage().getInfoValue("Number of Fibers")).
                setNumberOfConnections(fiberGroupPage.getModalWindowPage().getInfoValue("Connections")).
                setNumberOfSplices(fiberGroupPage.getModalWindowPage().getInfoValue("Splices")).
                setEnd1Name(fiberGroupPage.getModalWindowPage().getInfoValue("End 1 Name")).
                setEnd1Connector(fiberGroupPage.getModalWindowPage().getInfoValue("End 1 Connector")).
                setEnd2Name(fiberGroupPage.getModalWindowPage().getInfoValue("End 2 Name")).
                setEnd2Connector(fiberGroupPage.getModalWindowPage().getInfoValue("End 2 Connector")).
                build();
        return viewInfoFiberGroupData;
    }

    public FiberGroup getViewInfoFiberGroupData() {
        FiberGroup viewInfoFiberGroupData = FiberGroup.builder(fiberGroupPage.getModalWindowPage().getInfoValue("Name")).
                setComment(fiberGroupPage.getModalWindowPage().getInfoValue("Comment")).
                setFiberType(fiberGroupPage.getModalWindowPage().getInfoValue("Fiber Type")).
                setStartingFiberNumber(fiberGroupPage.getModalWindowPage().getInfoValue("Starting Fiber")).
                setNumberOfFibers(fiberGroupPage.getModalWindowPage().getInfoValue("Number of Fibers")).
                setNumberOfConnections(fiberGroupPage.getModalWindowPage().getInfoValue("Connections")).
                setNumberOfSplices(fiberGroupPage.getModalWindowPage().getInfoValue("Splices")).
                setEnd1Name(fiberGroupPage.getModalWindowPage().getInfoValue("End 1 Name")).
                setEnd1Connector(fiberGroupPage.getModalWindowPage().getInfoValue("End 1 Connector")).
                setEnd1ConnectorOption(fiberGroupPage.getModalWindowPage().getInfoValue("End 1 Link")).
                setEnd2Name(fiberGroupPage.getModalWindowPage().getInfoValue("End 2 Name")).
                setEnd2Connector(fiberGroupPage.getModalWindowPage().getInfoValue("End 2 Connector")).
                setEnd2ConnectorOption(fiberGroupPage.getModalWindowPage().getInfoValue("End 2 Link")).
                build();
        return viewInfoFiberGroupData;
    }

    @Step
    public void verifyViewFiberGroupInformationDetails(FiberGroup defaultFiberGroup) {
        assertThat(getViewInfoDefaultFiberGroupData(), samePropertyValuesAs(defaultFiberGroup));
    }

    @Step
    public void verifyViewModifiedFiberGroupInformationDetails(FiberGroup modifiedFiberGroup) {
        assertThat(getViewInfoFiberGroupData(), samePropertyValuesAs(modifiedFiberGroup));
    }

    @Step
    public void verifyCreateFiberGroupDetails(FiberGroup readFilterGroup, FiberGroup defaultFiberGroup) {
        assertThat(readFilterGroup, samePropertyValuesAs(defaultFiberGroup));
    }

    @Step
    public void clickOnTooltipButton() {
        fiberGroupPage.getFiberGroupModalWindow().clickOnTooltipButton();
    }

    @Step
    public void verifyTooltipTextCorrect(String contentText) {
        assertThat(fiberGroupPage.getFiberGroupModalWindow().getTooltipContent(), is(equalTo(contentText)));
    }

    @Step
    public void extendFiberGroupTextArea() {
        fiberGroupPage.getFiberGroupModalWindow().extendFiberGroupTextArea();
    }

    @Step
    public void enterFiberGroupPerLine(String... fiberGroupNames) {
        fiberGroupPage.getFiberGroupModalWindow().enterFiberGroupPerLine(fiberGroupNames);
    }

    @Step
    public void enterFiberGroupComment(String comment) {
        fiberGroupPage.getFiberGroupModalWindow().enterFiberGroupComment(comment);
    }

    @Step
    public void setStartingFiberNumberValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setStartingFiberNumberValue(value);
    }

    @Step
    public void setNumberOfFibersValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setNumberOfFibersValue(value);
    }

    @Step
    public void setConnectionsValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setConnectionsValue(value);
    }

    @Step
    public void isConnectionsCorrect(String value) {
        assertThat(fiberGroupPage.getFiberGroupModalWindow().getConnectionsCount(), is(equalTo(value)));
    }

    @Step
    public void setSplicesValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setSplicesValue(value);
    }

    @Step
    public void isSplicesValueCorrect(String value) {
        assertThat(fiberGroupPage.getFiberGroupModalWindow().getSplicesCount(), is(equalTo(value)));
    }


    @Step
    public void setEnd1NameValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setEnd1NameValue(value);
    }

    @Step
    public void setEndConnectorValueByName(String connectorName, String value) {
        fiberGroupPage.getFiberGroupModalWindow().setEndConnectorValueByName(connectorName, value);
    }

    @Step
    public void selectEndConnectorCheckBox(String connectorName, String value) {
        fiberGroupPage.getFiberGroupModalWindow().selectEndConnectorCheckBox(connectorName, value);
    }

    @Step
    public void setEnd2NameValue(String value) {
        fiberGroupPage.getFiberGroupModalWindow().setEnd2NameValue(value);
    }

    @Step
    public void isTooltipContentBlockPresent(boolean state) {
        assertThat(fiberGroupPage.getFiberGroupModalWindow().isTooltipContentBlockPresent(), is(state));
    }

    @Step
    public void isSaveButtonEnabled(boolean state) {
        assertThat(fiberGroupPage.getAddCollectionWindow().isSaveButtonEnabled(), is(state));
    }

    public void clickOnAddCollectionSaveButton() {
        fiberGroupPage.getAddCollectionWindow().clickOnSaveButton();
    }

    @Step
    public void clickOnEditCollectionButton() {
        fiberGroupPage.getAddCollectionWindow().clickOnEditCollectionButton();
        waitForModalWindowVisibility();
    }

    public void waitForModalWindowVisibility() {
        fiberGroupPage.getModalWindowPage().waitForModalWindowVisibility();
    }

    @Step
    public void enterCollectionName(String projectName) {
        fiberGroupPage.getAddCollectionWindow().enterProjectName(projectName);
    }

}
