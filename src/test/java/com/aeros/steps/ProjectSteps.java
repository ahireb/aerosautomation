package com.aeros.steps;

import com.aeros.model.Project;
import com.aeros.model.ProjectModel;
import com.aeros.pages.FiberGroupPage;
import com.aeros.pages.ProjectsDashboardPage;
import com.aeros.utils.DateUtils;

import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;
import net.thucydides.core.steps.ScenarioSteps;

import static com.aeros.constants.Const.DateFormat.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.hamcrest.core.Is.is;

import java.util.ArrayList;
import java.util.List;

import com.aeros.constants.*;
import com.aeros.constants.Const.DataModel;

public class ProjectSteps extends ScenarioSteps {
    private ProjectsDashboardPage dashboardPage;
  //  private FiberGroupPage fiberGroupPage;
    private Project project;

    public void clicksCreateNewProjectButton() {
        dashboardPage.getProjectsFilterTab().clickOnCreateNewProjectButton();
    }

    @Step
    public void createProjectsFromDatafile()
    {
    	List<String> records = new ArrayList<String>();
    	ProjectModel pm = new ProjectModel(DataModel.PROJECT_MODEL_FILE);
    	records = pm.readProjectFile();
    	
    }
    
       
    @Step
    public void isModalContentDisplayed(){
        assertThat(dashboardPage.getProjectsModalWindow().isModalContentBlockDisplayed(), is(true));
    }

    @StepGroup
    public void mergeProjects(String firstProjectName, String secondProjectName, String mergedProjectName) {
        selectProjectToMerge(firstProjectName);
        verifyProjectToMergeSelected(firstProjectName, true);
        selectProjectToMerge(secondProjectName);
        verifyProjectToMergeSelected(firstProjectName, true);
        renameNewProjectNameForMerging(mergedProjectName);
        clickOnProjectSubmitButton();
        waitForProjectNameTextToAppear(mergedProjectName);
    }
    
    @Step 
    public void checkProjectListPresent()
    {
    	dashboardPage.verifyProjectListPresent();
    }
    @Step
    public void checkHelpPagePresent()
    {
    	dashboardPage.verifyHelpPagePresent();
    }
    
    @Step
    public void clickToCloseHelpPage()
    {
    	dashboardPage.toCloseHelpPage();
    }
    
    @Step
    public void selectActiveFromDropDownList(String option)
    {
    	dashboardPage.clickOnActiveFromDropDownList(option);
    }

    @Step
    private void renameNewProjectNameForMerging(String mergedProjectName) {
        dashboardPage.getProjectsModalWindow().getNewProjectNameWebElement().clear();
        dashboardPage.getProjectsModalWindow().getNewProjectNameWebElement().sendKeys(mergedProjectName);
    }

    @Step
    private void selectProjectToMerge(String firstProject) {
        dashboardPage.getProjectsModalWindow().selectProjectToMerge(firstProject);
    }

    @Step
    private void verifyProjectToMergeSelected(String firstProject, boolean state) {
        assertThat(dashboardPage.getProjectsModalWindow().isProjectForMergingSelected(firstProject), is(state));
    }
    


    @StepGroup
    public void clickMergeProjectButton() {
        dashboardPage.getProjectsFilterTab().clickOnMergeProjectButton();
        assertThat(dashboardPage.getProjectsModalWindow().isModalContentBlockDisplayed(), is(true));
    }

    @Step
    public void verifyMergeProjectButtonPresence(boolean state) {
        assertThat(dashboardPage.getProjectsFilterTab().isMergeProjectButtonPresent(), is(state));
    }

    @StepGroup
    public void selectFilterOptionByText(String option) {
        selectFilterOptionByValue(option);
        dashboardPage.getProjectsFilterTab().waitForProjectStatusValueAppeared(option);
        verifyProjectStatus(option);
    }

    @Step
    private void selectFilterOptionByValue(String option) {
        dashboardPage.getProjectsFilterTab().selectFilterOptionByValue(option);

    }

    @Step
    public ProjectSteps enterProjectName(String projectName) {
        dashboardPage.getProjectsModalWindow().enterProjectName(projectName);
        return this;
    }

    @Step
    public ProjectSteps enterProjectComment(String projectComment) {
        dashboardPage.getProjectsModalWindow().enterProjectComment(projectComment);
        return this;
    }

    @Step
    public void verifyModalWindowProjectNameValue(String value) {
        assertThat(dashboardPage.getProjectsModalWindow().getProjectNameValue(), is(equalTo(value)));
    }

    @Step
    public void clearModalWindowProjectCommentValue() {
        dashboardPage.getProjectsModalWindow().clearProjectCommentField();
    }

    @Step
    public void clickOnProjectSubmitButton() {
        dashboardPage.getProjectsModalWindow().clickOnProjectSubmitButton();
    }

    public void clickOnCreateProjectSubmitButton() {
        dashboardPage.getProjectsModalWindow().clickOnCreateProjectSubmitButton();
    }

    public void waitForProjectNameTextToAppear(String text) {
        dashboardPage.getProjectTableComponent().waitForProjectNameTextAppeared(text);
    }

    public void waitForProjectNameTextToDisappear(String text) {
        dashboardPage.getProjectTableComponent().waitForProjectAbsence(text);
    }

    @StepGroup
    public void createAProject(String projectName, String comment) {
        clicksCreateNewProjectButton();
        enterProjectName(projectName).enterProjectComment(comment).clickOnProjectSubmitButton();
        waitForProject(projectName);
        verifyProjectNamePresence(projectName);
    }

    @StepGroup
    public void createAProject(String projectName) {
        clicksCreateNewProjectButton();
        enterProjectName(projectName);
        clickOnCreateProjectSubmitButton();
        waitForProject(projectName);
        verifyProjectNamePresence(projectName);
    }

    @StepGroup
    public void makeProjectOperation(String projectName, String projectOption, String confirmButton) {
        clickOnProjectOptionButton(projectName, projectOption);
        verifyModalWindowProjectName(projectName);
        clickOnModalWindowButton(confirmButton);
        waitForProjectNameDisappear(projectName);
    }
    @Step
    public void checkProjectProgressBar()
    {
    	dashboardPage.checkProgressbar();
    }

    @Step
    public void verifyProjectNamePresence(String projectName) {
        assertThat(dashboardPage.getProjectTableComponent().isProjectNamePresent(projectName), is(true));
    }

    public void waitForProject(String projectName) {
        dashboardPage.getProjectTableComponent().waitForProjectName(projectName);
    }

    public void waitForProjectAbsence(String projectName) {
        dashboardPage.getProjectTableComponent().waitForProjectAbsence(projectName);
    }


    @Step
    public void verifyProjectCommentPresence(String projectName, boolean state) {
        assertThat(dashboardPage.getProjectTableComponent().isProjectCommentPresent(projectName), is(state));
    }


    @Step
    public void verifyProjectStatus(String projectNameStatus) {
        assertThat(dashboardPage.getProjectsFilterTab().getProjectStatusValue(), is(equalTo(projectNameStatus)));
    }

    @Step
    public void enterValueIntoSearchField(String value) {
        dashboardPage.getProjectsFilterTab().enterValueIntoSearchField(value);
    }

    @Step
    public void verifyProjectNamePresence(boolean state, String... projectNames) {
        assertThat(dashboardPage.getProjectTableComponent().isProjectNamePresent(projectNames), is(state));
    }

    public Project loadAdditionalInfo(String projectName) {
        Project project = new Project(projectName);
        project.setProjectName(projectName);
        project.setProjectCreator(dashboardPage.getHeaderTab().getNavigationTab().getUserNameLabel());
        project.setProjectStatus(dashboardPage.getProjectsFilterTab().getProjectStatusValue().toUpperCase());
        project.setComment(dashboardPage.getProjectTableComponent().getProjectComment(projectName));
        project.setCurrentDate(DateUtils.getNewCustomDate(project.getCurrentDate(),
                SLASH_FORMAT, PROJECT_INFO_DATE_FORMAT));
        return project;
    }

    @Step("Verify created project date regarding current date")
    public void verifyCurrentCreatedProjectDate(Project project) {
        assertThat(dashboardPage.getProjectTableComponent().getProjectCreationTime(project.getProjectName()),
                is(equalTo(project.getCurrentDate())));
    }

    @Step("Verify updated project date regarding current date")
    public void verifyCurrentUpdatedProjectDate(Project project) {
        assertThat(dashboardPage.getProjectTableComponent().getProjectUpdatedTime(project.getProjectName()),
                is(equalTo(project.getCurrentDate())));
    }

    @Step
    public void verifyProjectDetailsIdentity(Project project, Project comparedProject) {
        assertThat(project, samePropertyValuesAs(comparedProject));
    }

    @Step
    public void verifyProjectCommentCorrect(Project project) {
        assertThat(dashboardPage.getProjectTableComponent().getProjectComment(project.getProjectName()), is(equalTo(project.getComment())));
    }

    private void waitForProjectNameDisappear(String projectName) {
        dashboardPage.waitForProjectNameTextDisappeared(projectName);
    }

    public void clickOnProjectOptionButton(String projectName, String projectOption) {
        dashboardPage.getProjectTableComponent().clickOnProjectOptionButton(projectName, projectOption);
    }

    @Step
    public void goToProjectFiberGroupPage(String projectName) {
        dashboardPage.getProjectTableComponent().goToAProjectFiberGroupPage(projectName);

    }

    @Step
    public void verifyProjectIsRemoved(String projectName) {
        assertThat(dashboardPage.getProjectTableComponent().isProjectNamePresent(projectName), is(false));
    }

    @Step("Clicks on modal window button")
    public void clickOnModalWindowButton(String button) {
        dashboardPage.getModalWindow().clickOnModalWindowButton(button);
    }

    @Step("Verify modal window title")
    public void verifyModalWindowTitle(String text) {
        assertThat(dashboardPage.getModalWindow().getModalWindowTitle(), is(equalTo(text)));
    }


    @Step("Verify modal window project name {0}")
    public void verifyModalWindowProjectName(String projectName) {
        assertThat(dashboardPage.getModalWindow().getDeleteWindowProjectNameText(), is(equalTo(projectName)));
    }

    private String getProjectInfoBlockProjectName() {
        return dashboardPage.getProjectInfoModalWindow().getProjectInfoTitle();
    }

    private String getProjectInfoBlockValue(String key) {
        return dashboardPage.getProjectInfoModalWindow().getProjectInfoValue(key);
    }

    @Step
    public Project getProjectInfoBlockDetails() {
        Project project = new Project();
        project.setProjectName(getProjectInfoBlockProjectName());
        project.setComment(getProjectInfoBlockValue("Comment"));
        project.setProjectStatus(getProjectInfoBlockValue("Status").toUpperCase());
        project.setProjectCreator(getProjectInfoBlockValue("Created By"));
        project.setCurrentDate(DateUtils.getNewCustomDate(getProjectInfoBlockValue("Created Date"),
                "MMM d, yyyy", "MMM dd, yyyy"));
        return project;
    }

    @Step
    public void verifyProjectTabButtonPresence(String projectName, String button, boolean state) {
        assertThat(dashboardPage.getProjectTableComponent().isProjectTabButtonPresent(projectName, button), is(state));
    }

    @Step
    public void verifyProjectsListSize(int size) {
        dashboardPage.getProjectTableComponent().waitForProjectSize(size);
        assertThat(dashboardPage.getProjectTableComponent().getProjectSize(), is(equalTo(size)));
    }

    public void waitForModalWindowVisibility() {
        dashboardPage.getModalWindow().waitForModalWindowVisibility();
    }
}

