package com.aeros.steps;
import com.aeros.constants.Const;
import com.aeros.pages.ToolsPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class ToolSteps  extends ScenarioSteps {
	
	private ToolsPage toolsPage; 

	@Step
	public void selectToolAddAction() {
       		toolsPage.clickOnToolsTabButton();
		/* First select tools option   */
		     
    }
	
	
	@Step
	public void selectToolFromDropDown(String tool) {
       		toolsPage.selectToolType(tool);
		/* First select tools option   */
		     
    }
	@Step
	public void clickOnLinkPassFailDownArrow()
	{
		toolsPage.clickOnLinkPassFailDownArrow();
	}
	
	@Step
	public void selectLCFromDropDown(String lc) {
       		toolsPage.selectLaunchCable(lc);
		/* First select tools option   */
		     
    }
	@Step
	public void selectRCFromDropDown(String rc)
	{
		toolsPage.selectReceiveCable(rc);
	}
	@Step
	public void selectTool() {
   		toolsPage.clickOnToolsAddButton();
	
}
	@Step
	public void selectEventStartConnector(String option)
	{
		toolsPage.selectEventStartConnector(option);
	}
	
	@Step
	public void selectEventEndConnector(String option)
	{
		toolsPage.selectEventEndConnector(option);
	}
	@Step
	public void selectLinkStartConnector(String option)
	{
		toolsPage.selectEventEndConnector(option);
	}
	@Step
	public void selectLinkEndConnector(String option)
	{
		toolsPage.selectEventEndConnector(option);
	}
	
	@Step
	public void selectSingleMode() {
   		toolsPage.clickOnAddSingleMode();
	
}
	@Step
	public void organizationalMenuPresent()
	{
		toolsPage.organizationalMenuPresent();
	}
	
	
	@Step
	public void selectMultiMode()
	{
		toolsPage.clickOnAddMultiMode();
	}
		
	public void addOTDRToolName(String toolname) {
   		toolsPage.addOTDRToolName(toolname);
	}
	
	@Step
	public void addInspectionToolName(String toolname) {
   		toolsPage.addTestName(toolname);
	}
	
	@Step
	public void clickOnTraceButton()
	{
		toolsPage.clickOnTraceButton();
	}
	@Step
	public Object[] scrollUpWindow()
	{
		toolsPage.scrollUPWindow();
		return null;
	}
 
   	
   	@Step
   	public void selectHelpButton()
   	{
   		toolsPage.clickOnHelpButton();
   	}

	@Step
	public void selectBothEndsbutton() {
   		toolsPage.clickOnBothEnds();
	
}
	
	@Step
	public void selectOnlyEnd1button()
	{
		toolsPage.clickOnOnlyEnd1();
	}
	
	@Step
	public void selectOnlyEnd2Button()
	{
		toolsPage.clickOnlyEnd2();
	}
	
	@Step
	public void selectIEC45Rule() {
   		toolsPage.clickOnIEC45Rule();
	
}
	@Step
	public void selectNonePassFailStandard()
	{		toolsPage.clickOnNonePassFailStandard();
	}
	
	@Step
	public void selectIECAPCRule()
	{
		toolsPage.clickOnIECAPCRule();
	}
	
	@Step
	public void selectIEC26Rule()
	{
		toolsPage.clickOnIEC26Rule();
	}
	
	@Step
	public void selectATandTRule()
	{
		toolsPage.clickOnATandTRule();
	}
	
	@Step
	public void selectIPCRule()
	{
		toolsPage.clickOnIPCRule();
	}
	
	@Step
	public void selectIECPCMMRule()
	{
		toolsPage.clickOnIECPCMM();
	}
	
	@Step
	public void selectAT_TMM()
	{
		toolsPage.clickOnAT_TMM();
	}
	
	@Step
	public void selectIPCMM()
	{
		toolsPage.clickOnIPCMM();
	}
	
	@Step
	public void clickOnOTDRToolSaveButtonToSave() {
   		toolsPage.clickOnOTDRToolSaveButton();
   		}	
	
	@Step
	public void waitForToolVisibility(String name) {
   		toolsPage.waitForToolVisibility(name);
   		}	
	
	@Step
	public void waitForLCCableLength()
	{
		toolsPage.waitForLCLengthVisibility();
	}
	@Step
	public void waitForRCCableLength()
	{
		toolsPage.waitForRCLengthVisibility();
	}
	
	@Step
	public void waitForResultButton()
	{
		toolsPage.waitForResultButtonVisibility();
	}
	
	@Step
	public void enterLCLength(String val)
	{
		toolsPage.enterLCLength(val);
	}
	@Step
	public void enterRCLength(String val)
	{
		toolsPage.enterRCLength(val);
	}
	@Step 
	public void isToolVisible(String name) {
		
		/*Scroll */
   		toolsPage.isToolVisible(name);
   		}	
	
	@Step
	public void selectToolByName(String name) {
   		toolsPage.selectToolByName(name);
   		}	
	
	@Step
	public void clickOnSaveButtonToSave() {
   		toolsPage.clickOnSaveButton();
   		
   		
	
}
	@Step
	public void clickOnInformationButton() {
   		toolsPage.clickOnIbutton();
	
}
	@Step
	public void enterDescription(String des)
	{
		toolsPage.enterDescriptionForOLTSTool(des);
	}
	
	@Step
	public void selectBidirectionalOptionForOLTS()
	{
	   toolsPage.selectBidirectionalOption();
	   
	}
	
	@Step
	public void aeRosLogoPresent()
	{
	   toolsPage.aeRosLogoPresent();
	   
	}
	@Step
	public void clickOnResultButton()
	{
		toolsPage.clickOnResultButton();
	}
	
	@Step
	public void selectOneCordReferenceOptionForOLTS()
	{
		toolsPage.selectOneCordreferenceOption();
	}
}	
	

