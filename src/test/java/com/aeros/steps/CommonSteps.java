package com.aeros.steps;

import com.aeros.pages.CommonPage;
import com.aeros.steps.CommonSteps;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class CommonSteps extends ScenarioSteps {
    private CommonPage commonPage;

    @Step
    public void verifyErrorNotificationMessage(String text) {
        commonPage.waitForErrorNotificationMessage();
        assertThat(commonPage.getErrorNotificationMessage(), is(equalTo(text)));
    }

    @Step
    public void verifyErrorNotificationMessage(String text, String formattedValue) {
        commonPage.waitForErrorNotificationMessage();
        assertThat(commonPage.getErrorNotificationMessage(), is(equalTo(String.format(text, formattedValue))));
    }

    @Step
    public void verifySuccessfulNotificationMessage(String text, String formattedValue) {
        commonPage.waitForSuccessfulNotificationMessage();
        assertThat(commonPage.getSuccessfulMessageText(), is(equalTo(String.format(text, formattedValue))));
    }

    @Step
    public void verifySuccessfulNotificationMessage(String text) {
        commonPage.waitForMessageAppearance(text);
        assertThat(commonPage.verifySuccessfulMessageIsDisplayed(text), is(true));
    }

    public void waitForSuccessfulNotificationMessageInvisibility() {
        commonPage.waitForSuccessfulNotificationMessageInvisibility();
    }

    public boolean isErrorMessageBlockPresent() {
        return commonPage.isErrorMessageBlockPresent();
    }

}
