package com.aeros.steps;

import com.aeros.model.UIReportFile;

import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UIReportSteps  extends ScenarioSteps {
	
	private UIReportFile uiReport; 
	
	public void openUIReportFile(String filename)
	{
		uiReport = new UIReportFile(filename);
	}
	@Step
    public void UI_STEP_INFO(String str)
    {
    	
    }
    
    @Step 
    @Screenshots(onlyOnFailures=true)
    public void UI_STEP(String step, String step_info)
    {
    	//UI_STEP_INFO(step_info);
    }
    
    @Step      
    public void UI_STEP(String step)
    {
    	
    }
    
    @Step      
    public void UI_STEP_FAILURE(String step)
    {
    	
    }
    public boolean isUITestPassed(String res)
    {
    	if( res.equals("FAIL"))
    		return false;
    	
    	return true;
    }
    
    @Step
    public void UI_Steps()
    {
    	String line;
    	String title;
    	String step_info;
    	String result;
    	String result_failure;

    	
    	while(true)
    	{
    		/* Read STEP title */
    		line = uiReport.getNextLine();
    		
    		if( line == null)
    			break;
    		
    		title = uiReport.getStepTitle(line);
    		if( title == null)
    		{
    			System.out.println("Invalid UI report file\n");
    			return;
    		}
    		
    		/*Read step info */
    		line =  uiReport.getNextLine();
    		if( line == null)
    			break;
  		
   			step_info = uiReport.getStepInfo(line);
   			if( step_info == null)
   			{
   				System.out.println("Invalid UI report file\n");
   				return;
   			}
   			
   			step_info = "\n" + step_info;
   			UI_STEP(title, step_info);
   			/*Read step result*/
   			line =  uiReport.getNextLine();
    		if( line == null)
    			break;
  		
   			result = uiReport.getStepResult(line);
   			if( result == null)
   			{
   				System.out.println("Invalid UI report file\n");
   				return;
   			}
    		
   			if( result.trim().equals("FAIL"))
   			{
   				/* Read result failure reason*/
   				line =  uiReport.getNextLine();
   	    		if( line == null)
   	    			break;
   	    		result_failure = uiReport.getStepResultFailureInfo(line);
   	    		if( result_failure == null)
   	   			{
   	   				System.out.println("Invalid UI report file\n");
   	   				return;
   	   			}
   	    		
   	    		UI_STEP_FAILURE(result_failure);
   	    		assertThat(isUITestPassed(result), is(true));
   	    		
   			}
   			
   			
   				
  	
    	}
    	
    }

}
