package com.aeros.pages;


import com.aeros.model.FiberGroup;
import com.aeros.pages.component.AddCollectionWindow;
import com.aeros.pages.component.EditFiberGroupWindow;
import com.aeros.pages.component.FiberGroupActionsPanel;
import com.aeros.pages.component.FiberGroupModalWindow;
import com.aeros.pages.component.HeaderComponent;
import com.aeros.pages.component.ModalWindowPage;
import com.aeros.pages.component.ProjectCollectionWindow;
import com.aeros.pages.component.ProjectsNavBarComponent;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ch.lambdaj.Lambda.extract;
import static ch.lambdaj.Lambda.on;

public class FiberGroupPage extends WebBasePage {
    public FiberGroupPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".panel-heading span")
    private List<WebElement> panelHeadingLabels;

    @FindBy(css = "#fiber-groups .fiberGroup:not(#project-row)")
    private List<WebElement> fiberGroupList;

    @FindBy(css = ".btn-create-fiberGroup > img")
    private List<WebElement> mediaTypeList;

    @FindBy(css = "#project-row .actions .btn-upload")
    private WebElement uploadProjectCollectionButton;

    @FindBy(css = "#project-row .applied-test-setup-button .count")
    private WebElement attachedProjectFileCounter;

    @FindBy(css = "#project-row .remove-test-setup-button")
    private WebElement removeProjectCollectionButton;

    @FindBy(css = "#project-row .applied-test-setup-button")
    private WebElement projectCollectionButton;

    @FindBy(css = ".applied-test-setup-button > span:last-of-type")
    private WebElement projectCollectionNameText;

    @FindBy(id = "createTestSetupButtonGroup")
    private WebElement testSetupButton;
    
    @FindBy(xpath = "html/body/div[1]/div[1]/div[2]/div/div/div[2]/div/div[1]/div[2]/div[2]/button[6]")
    private WebElement addFGButton;
       
    
    
    public boolean isAppliedProjectCollectionButtonPresent() {
        return isElementPresent(By.cssSelector("#project-row .applied-test-setup-button"));
    }

    public void waitForAppliedProjectCollectionButtonAbsence() {
        waitForAbsenceOf("#project-row .applied-test-setup-button");
    }

    public FiberGroupActionsPanel getFiberGroupActionsPanel() {
       return new FiberGroupActionsPanel(getDriver());
   }
   // public FiberGroupAddAction()
   // {
   // 	addFGButton.click();
    //}

    public ModalWindowPage getModalWindowPage() {
        return new ModalWindowPage(getDriver());
    }

    public FiberGroupModalWindow getFiberGroupModalWindow() {
        return new FiberGroupModalWindow(getDriver());
    }

    public ProjectCollectionWindow getProjectCollectionWindow() {
        return new ProjectCollectionWindow(getDriver());
    }

    public AddCollectionWindow getAddCollectionWindow() {
        return new AddCollectionWindow(getDriver());
    }

    public EditFiberGroupWindow getEditFiberGroupWindow() {
        return new EditFiberGroupWindow(getDriver());
    }

    public HeaderComponent getHeaderTab() {
        return new HeaderComponent(getDriver());
    }

    public ProjectsNavBarComponent getProjectsNavBarComponent() {
        return new ProjectsNavBarComponent(getDriver());
    }

    public List<String> getPanelHeadingLabels() {
        return extract(panelHeadingLabels, on(WebElement.class).getText());
    }

    public boolean isFiberGroupPresent(FiberGroup fiberGroup) {
        String fiberGroupNameXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]";
        return isElementPresent(By.xpath(String.format(fiberGroupNameXpath, fiberGroup.getFiberGroupName().trim())));
    }

    public By getFiberGroupBy(FiberGroup fiberGroup) {
        String fiberGroupNameXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]";
        return find(By.xpath(String.format(fiberGroupNameXpath, fiberGroup.getFiberGroupName())));
    }

    public void waitForFiberGroupToBeAbsent(FiberGroup fiberGroup) {
        waitForAbsenceOf(String.format("//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]", fiberGroup.getFiberGroupName()));
    }

    public void waitForFiberGroupToBePresent(FiberGroup fiberGroup) {
        //String fiberGroupNameXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]";
    	//Changed by Bhakti
        String fiberGroupNameXpath ="//div[contains(@class,'fiber-group-name ng-binding') and normalize-space(text()) = \"%s\"]";
        waitForElementVisibility(find(By.xpath(String.format(fiberGroupNameXpath, fiberGroup.getFiberGroupName().trim()))));
    }

    public void selectFiberGroupAction(String fiberGroupName, String action) {
        String fiberGroupActionXpath = "//div[contains(@class,'fiber-group-name')][contains(normalize-space(),\"%s\")]/following-sibling::div[@class='actions']/button[contains(@class,'%s')]";
        waitForElementToBeClickable(find(By.xpath(String.format(fiberGroupActionXpath, fiberGroupName, action))));
        clickOn(find(By.xpath(String.format(fiberGroupActionXpath, fiberGroupName, action))));
    }

    public void selectFiberGroupByName(String fiberGroupName) {
        //String fiberGroupActionXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]";
        String fiberGroupActionXpath ="//div[contains(@class,'fiber-group-name ng-binding') and normalize-space(text()) = \"%s\"]";
        clickOn(find(By.xpath(String.format(fiberGroupActionXpath, fiberGroupName))));
    }
    
    /*
    public void clickResultButtonByFiberGroupName(String fiberGroupName) {
    	
    	   	
    	String fiberGroupActionXpath ="//div[contains(@class,'fiber-group-name')][normalize-space(text()) = \"%s\"]/parent::div/parent::div/div[3]/button[4]";
    	
    	
    }
    
    */

    public boolean isFiberGroupSelected(String fiberGroupName) {
        String fiberGroupActionXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]/ancestor::div[contains(@class,'selected')]";
        return isElementPresent(By.xpath(String.format(fiberGroupActionXpath, fiberGroupName)));
    }

    public boolean isFiberGroupCollectionButtonPresent(FiberGroup fiberGroup) {
        String fiberGroupNameXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]/parent::div//div[contains(@class,'filecollection')]";
        return isElementPresent(By.xpath(String.format(fiberGroupNameXpath, fiberGroup.getFiberGroupName().trim())));
    }


    public int getFiberGroupListSize() {
        return fiberGroupList.size();
    }

    public Set<String> getSelectedFiberGroupNames() {
        Set<String> fiberGroupNames = new HashSet<>();
        for (WebElement element : fiberGroupList) {
            if (element.getAttribute("class").contains("selected")) {
                fiberGroupNames.add(element.findElement(By.cssSelector(".fiber-group-name")).getText());
            }
        }
        return fiberGroupNames;
    }

    public void selectMediaTypeByName(String mediaType) {
        clickViaJS(find(By.cssSelector(String.format(".btn-create-fiberGroup > [alt='%s']", mediaType))));
    }

    public List<String> getMediaTypeNames() {
        return extract(mediaTypeList, on(WebElement.class).getAttribute("alt"));
    }

    public void clickOnProjectUploadButton() {
        clickOn(uploadProjectCollectionButton);
    }

    public int getNumberOfAttachedDocuments() {
        return Integer.parseInt(attachedProjectFileCounter.getText());
    }

    public int getNumberOfFiberGroupAttachedDocuments(FiberGroup fiberGroup) {
        return Integer.parseInt(getFiberGroupCollectionElement(fiberGroup).findElement(By.cssSelector(" .count")).getText());
    }

    public void clickOnFiberGroupCollection(FiberGroup fiberGroup) {
        clickViaJS(getFiberGroupCollectionElement(fiberGroup).findElement(By.cssSelector(" .applied-test-setup-button")));
    }

    public String getFiberGroupCollectionNameText(FiberGroup fiberGroup) {
        return getFiberGroupCollectionElement(fiberGroup).findElement(By.cssSelector(" .applied-test-setup-button > span:last-of-type")).getText();
    }

    public void clickOnDeleteFiberGroupCollectionButton(FiberGroup fiberGroup) {
        clickViaJS(getFiberGroupCollectionElement(fiberGroup).findElement(By.cssSelector(" .btn-remove")));
    }


    public WebElement getFiberGroupCollectionElement(FiberGroup fiberGroup) {
        String fiberGroupNameXpath = "//div[contains(@class,'fiber-group-name')][normalize-space()=\"%s\"]/parent::div//div[contains(@class,'filecollection')]";
        return find(By.xpath(String.format(fiberGroupNameXpath, fiberGroup.getFiberGroupName())));
    }

    public void clickOnRemoveProjectCollectionButton() {
        waitForElementToBeClickable(removeProjectCollectionButton);
        clickOn(removeProjectCollectionButton);
    }

    public void clickOnProjectCollectionButton() {
        clickOn(projectCollectionButton);
    }

    public String getProjectCollectionNameText() {
        return projectCollectionNameText.getText();
    }

    public void clickOnTestSetupButton() {
        clickOn(testSetupButton);
    }
    
   

    public void selectSingleModeType(String testSetupType) {
        String xpath = "//strong[text()='Single mode']/parent::li/following-sibling::li[@class='divider']/preceding-sibling::li/a[text()='%s']";
        clickOn(find(By.xpath(String.format(xpath, testSetupType))));
    }


}
