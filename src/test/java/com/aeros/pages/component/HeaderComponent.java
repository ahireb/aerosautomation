package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import com.aeros.utils.PatternMatcherUtils;
//import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.aeros.constants.Const.RegExp.*;

public class HeaderComponent extends WebBasePage{

    public HeaderComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".header h2 > span:first-child")
    private WebElement headerPageLabel;

    @FindBy(id = "page-instructions")
    private WebElement pageInstructionsButton;

    public NavigationBarTab getNavigationTab(){
        return new NavigationBarTab(getDriver());
    }

    public String getHeaderPageLabel(){
        return headerPageLabel.getText();
    }

    public void clickOnPageInstructionsButton(){
        clickOn(pageInstructionsButton);
    }

    public void waitUntilTextToDisappear(String text){
        waitForTextToDisappear(headerPageLabel, text);
    }

    public String getProjectInfoTitle() {
        return PatternMatcherUtils.getMatcherString(getHeaderPageLabel(), TEXT_AFTER_COLON_EXPRESSION, 0).trim();
    }

}
