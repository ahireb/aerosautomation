package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProjectFilterTabComponent extends WebBasePage {
    public ProjectFilterTabComponent(WebDriver driver) {
        super(driver);
    }

    private ProjectModalWindowPage projectModalWindowPage;

    @FindBy(css = ".btn-add")
    private WebElement addProjectButton;

    @FindBy(css = ".btn-merge")
    private WebElement mergeProjectButton;

    @FindBy(id = "status-filters")
    private WebElement statusFiltersDropDown;

    @FindBy(css = ".form-control.ng-pristine.ng-untouched.ng-valid.searching")
    private WebElement filterInput;

    public void clickOnCreateNewProjectButton() {
        clickOn(addProjectButton);
    }

    public void clickOnMergeProjectButton() {
        clickOn(mergeProjectButton);
    }

    @Step
    public void selectFilterOptionByValue(String option) {
        Select select = new Select(statusFiltersDropDown);
        select.selectByValue(option);
    }

    public boolean isMergeProjectButtonPresent() {
        return isElementPresent(By.cssSelector(".btn-merge"));
    }

    public String getProjectStatusValue() {
        return statusFiltersDropDown.getAttribute("value");
    }

    public void waitForProjectStatusValueAppeared(String value) {
        waitForTextValueToBeAppeared(statusFiltersDropDown, value);
    }

    public void enterValueIntoSearchField(String value) {
        enter(value).into(filterInput);
    }
}
