package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;

import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;

public class FiberGroupActionsPanel extends WebBasePage{
	
	@FindBy(css = "#actions .btn-add")
    private WebElement addFgButton;
	
    public FiberGroupActionsPanel(WebDriver driver) {
        super(driver);
    }

   public void selectFiberGroupAction(String option){
	   System.out.println("Inside selectFiberGroupAction\n");
	   
       waitForElementVisibility(find(By.cssSelector(String.format("#actions .btn-%s", option))));
       clickViaJS(find(By.cssSelector(String.format("#actions .btn-%s", option))));
       
       System.out.println("Inside selectFiberGroupAction Done\n");
   }

    public boolean isFiberGroupActionEnabled(String option){
        return find(By.cssSelector(String.format("#actions .btn-%s", option))).isEnabled();
    }

}
