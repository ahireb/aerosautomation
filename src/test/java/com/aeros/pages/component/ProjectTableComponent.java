package com.aeros.pages.component;

import com.aeros.pages.FiberGroupPage;
import com.aeros.pages.WebBasePage;
import com.aeros.utils.PatternMatcherUtils;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.aeros.constants.Const.RegExp.*;

public class ProjectTableComponent extends WebBasePage {

    public ProjectTableComponent(WebDriver driver) {
        super(driver);
    }
    public FiberGroupPage fiberGroupPage;

    @FindBy(xpath = "//div[@class='project-header']/following-sibling::div")
    private List<WebElement> projectList;

    public boolean isProjectNamePresent(String projectName) {
        String projectNameXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = \"%s\"]";
        return isElementPresent(By.xpath(String.format(projectNameXpath, projectName)));
    }

    public void waitForProjectName(String projectName){
        String projectNameXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = \"%s\"]";
        waitForElementVisibility(find(By.xpath(String.format(projectNameXpath, projectName))));
    }

    public void waitForProjectAbsence(String projectName){
        String projectNameXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = \"%s\"]";
        waitForAbsenceOf(String.format(projectNameXpath, projectName));
    }

    public boolean isProjectCommentPresent(String projectName) {
        String projectCommentXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = \"%s\"]/span[contains(@class,'comment')]";
        return isElementPresent(By.xpath(String.format(projectCommentXpath, projectName)));
    }


    public boolean isProjectNamePresent(String... projectNames) {
        String projectNameXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = \"%s\"]";
        for (String projectName : projectNames) {
            if (!isElementPresent(By.xpath(String.format(projectNameXpath, projectName)))) {
                return false;
            }
        }
        return true;
    }

    public String getProjectCreationTime(String projectName) {
        String projectCreationTimeXpath = "//div[normalize-space(text()) = '%s']/following-sibling::div[contains(@class,'project-creation-time')]";
        return find(By.xpath(String.format(projectCreationTimeXpath, projectName))).getText();
    }

    public String getProjectUpdatedTime(String projectName) {
        String projectCreationTimeXpath = "//div[normalize-space(text()) = '%s']/following-sibling::div[@class='project-update-time']/span";
        return find(By.xpath(String.format(projectCreationTimeXpath, projectName))).getText();
    }

    public void clickOnDeleteProjectButton(String projectName) {
        String projectDeleteButtonXpath = "//div[normalize-space(text()) = '%s']/parent::div//button[contains(@class,'btn-hide')]";
        clickOn(find(By.xpath(String.format(projectDeleteButtonXpath, projectName))));
    }

    public void clickOnProjectOptionButton(String projectName, String projectOption) {
        String projectDeleteButtonXpath = "//div[normalize-space(text()) = '%s']/parent::div//button[contains(@class,'btn-%s')]";
        clickViaJS(find(By.xpath(String.format(projectDeleteButtonXpath, projectName, projectOption))));
    }

    public boolean isProjectTabButtonPresent(String projectName, String button) {
        String projectDeleteButtonXpath = "//div[normalize-space(text()) = '%s']/parent::div//button[contains(@class,'btn-%s')]";
        return isElementPresent(By.xpath(String.format(projectDeleteButtonXpath, projectName, button)));
    }

    public void goToAProjectFiberGroupPage(String projectName) {
        String projectDeleteButtonXpath = "//div[normalize-space(text()) = '%s']";
        clickOn(getDriver().findElement(By.xpath(String.format(projectDeleteButtonXpath, projectName))));
    }

    public String getProjectComment(String projectName) {
        String projectCommentXpath = "//div[contains(@class,'project-name')][normalize-space(text()) = '%s']//span";
        String comment = find(By.xpath(String.format(projectCommentXpath, projectName))).getText();
        return PatternMatcherUtils.getMatcherString(comment,
                TEXT_INSIDE_BRACKETS_EXPRESSION, FIRST_GROUP_INDEX);
    }

    public int getProjectSize() {
        return projectList.size();
    }

    public void waitForProjectSize(final int size) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 5);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return (Boolean) (getProjectSize() == size);
            }
        });
    }

}
