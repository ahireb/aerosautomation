package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProjectCollectionWindow extends WebBasePage{
    public ProjectCollectionWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".modal-body b")
    private WebElement projectAttachmentText;

    @FindBy(css = "[name='attachmentForm'] .modal-footer .btn")
    private WebElement closeButton;

    public String getProjectAttachmentText(){
        return projectAttachmentText.getText();
    }

    public void closeProjectCollectionWindow(){
        waitForElementToBeClickable(By.cssSelector("[name='attachmentForm'] .modal-footer .btn"));
        clickViaActions(closeButton);
    }

    public boolean isCloseButtonEnabled(){
        return closeButton.isEnabled();
    }
}
