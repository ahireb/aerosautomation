package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditFiberGroupWindow extends WebBasePage {
    public EditFiberGroupWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "fiberGroup")
    private WebElement fiberGroupField;

    @FindBy(css = "button[type='submit']")
    private WebElement submitButton;

    public void enterFiberGroupName(String fiberGroupName) {
        waitForElementVisibility(fiberGroupField);
        fiberGroupField.clear();
        enter(fiberGroupName).into(fiberGroupField);
    }

    public void clickOnSubmitButton() {
        clickOn(submitButton);
    }

}