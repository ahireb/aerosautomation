package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import com.aeros.utils.PatternMatcherUtils;
import net.serenitybdd.core.annotations.findby.By;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

//import static ch.lambdaj.Lambda.extract;
//import static ch.lambdaj.Lambda.on;
import static com.aeros.constants.Const.RegExp.*;

public class ModalWindowPage extends WebBasePage {

    public ModalWindowPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".modal-footer .btn-danger")
    private WebElement deleteProjectButton;

    @FindBy(css = ".modal-dialog")
    private WebElement modalWindowBlock;

    @FindBy(css = ".modal-dialog .modal-body li")
    private WebElement modalWindowProjectName;

    @FindBy(css = ".modal-title")
    private WebElement modalWindowTitle;

    @FindBy(css = ".modal-body li")
    private List<WebElement> modalWindowListOptions;

    public String getModalWindowTitle() {
        return modalWindowTitle.getText();
    }

    public void clickOnModalWindowButton(String buttonText) {
        String modalWindowButtonXpath = "//div[@class='modal-content']//button[text()='%s']";
        clickOn(find(By.xpath(String.format(modalWindowButtonXpath, buttonText))));
    }

    public String getDeleteWindowProjectNameText() {
        return PatternMatcherUtils.getMatcherString(modalWindowProjectName.getText(),
                TEXT_INSIDE_QUOTATIONS_EXPRESSION, FIRST_GROUP_INDEX);
    }

    public boolean isModalContentBlockDisplayed() {
        return modalWindowBlock.isDisplayed();
    }

    public Set<String> getModalWindowSetOptions() {
        Set<String> modalWindowSetOptions = new HashSet<>();
        for (WebElement element : modalWindowListOptions) {
            modalWindowSetOptions.add(PatternMatcherUtils.getMatcherString(element.getText(),
                    TEXT_INSIDE_QUOTATIONS_EXPRESSION, FIRST_GROUP_INDEX));
        }
        return modalWindowSetOptions;
    }

    public void clickOnModalWindowButtonByText(String text) {
        clickViaJS(find(By.xpath(String.format("//button[text()='%s']", text))));
    }

    public String getInfoValue(String key) {
        String field = find(By.xpath(String.format("//b[text()='%s:']/parent::li", key))).getText();
        return PatternMatcherUtils.getMatcherString(field, TEXT_AFTER_COLON_EXPRESSION, 0).trim();
    }

    public void waitForModalWindowVisibility() {
        waitForElementVisibility(modalWindowBlock);
    }
}
