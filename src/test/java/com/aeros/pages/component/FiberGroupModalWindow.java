package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import com.aeros.utils.PatternMatcherUtils;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.*;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


import static com.aeros.constants.Const.RegExp.*;

public class FiberGroupModalWindow extends WebBasePage {

    public FiberGroupModalWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "fiberGroup")
    private WebElement fiberGroupField;

    @FindBy(id = "fiberGroupMultiple")
    private WebElement fiberGroupMultipleField;

    @FindBy(id = "fiberGroupComment")
    private WebElement fiberGroupComment;

    @FindBy(css = ".btn[type='submit']")
    private WebElement createFiberGroupButton;

    @FindBy(id = "fiberCount")
    private WebElement fiberCount;
    
    @FindBy(css = "[ng-model$='fiberType']")
    private WebElement fiberTypeDropDown;

    @FindBy(id = "startFiber")
    private WebElement startingFiberNumber;

    @FindBy(id = "fiberCount")
    private WebElement numberOfFibers;

    @FindBy(id = "connectionCount")
    private WebElement connectionsCount;

    @FindBy(id = "spliceCount")
    private WebElement spliceCount;

    @FindBy(id = "end1Name")
    private WebElement end1Name;

    @FindBy(id = "end2Name")
    private WebElement end2Name;

    @FindBy(css = ".showMoreAction > span")
    private WebElement showMoreLink;

    @FindBy(css = ".showMoreAction button")
    private WebElement showMoreTooltipButton;

    @FindBy(css = ".popover-content")
    private WebElement tooltipContent;

    public void enterFiberGroupName(String fiberGroupName) {
        enter(fiberGroupName).into(fiberGroupField);
    }

    public void enterMultipleFiberGroupName(String fiberGroupName) {
        enter(fiberGroupName).into(fiberGroupMultipleField);
    }

    public void setFiberCount(int cnt)
    {
    	String numStr = Integer.toString(cnt);
    	fiberCount.clear();
    	fiberCount.sendKeys(numStr);
    	
    }
    
    public void clickOnCreateFiberGroupButton() {
        createFiberGroupButton.submit();
    }

    public boolean isCreateFiberGroupButtonActive() {
        return isElementPresent(By.cssSelector(".btn[type='submit']:not([disabled='disabled'])"));
    }

    public void selectFiberTypeOptionByValue(String option) {
        Select select = new Select(fiberTypeDropDown);
        //select.selectByValue(String.format("string:%s", option));
        select.selectByVisibleText(option);
        
    }

    public String getConnectionsCount() {
        return connectionsCount.getAttribute("value");
    }

    public String getSplicesCount() {
        return spliceCount.getAttribute("value");
    }

    public String getStartingFiberNumber() {
        return startingFiberNumber.getAttribute("value");
    }

    public String getNumberOfFibers() {
        return numberOfFibers.getAttribute("value");
    }

    public String getEnd1ConnectorName() {
        return end1Name.getAttribute("value");
    }

    public String getEnd2ConnectorName() {
        return end2Name.getAttribute("value");
    }

    public String getEndConnectorValueByName(String connectorName) {
        return find(By.xpath(String.format("//span[text()='%s']/parent::label/following-sibling::div/select", connectorName))).getAttribute("value");
    }

    public String getEndConnectorSelectedValue(String connectorName) {
        String xpath = "//span[text()='%s']/parent::label/following-sibling::div/parent::div/following-sibling::div[position()=1]//input[contains(@class,'ng-not-empty')]/parent::label";
        return find(By.xpath(String.format(xpath, connectorName))).getText();

    }

    public String getFiberGroupNameValue() {
        return fiberGroupField.getAttribute("value");
    }

    public String getFiberTypeValue() {
        return PatternMatcherUtils.getMatcherString(fiberTypeDropDown.getAttribute("value"), TEXT_AFTER_COLON_EXPRESSION, 0);
    }

    public void extendFiberGroupTextArea() {
        clickOn(showMoreLink);
        waitForElementVisibility(fiberGroupMultipleField);
    }

    public void clickOnTooltipButton() {
        clickOn(showMoreTooltipButton);
    }

    public String getTooltipContent() {
        return tooltipContent.getText();
    }

    public void enterFiberGroupPerLine(String... fiberGroupNames) {
        String wholeText = "";
        for (String fiberGroupName : fiberGroupNames) {
            wholeText = wholeText + fiberGroupName + "\n";
        }
        enterMultipleFiberGroupName(wholeText);
    }

    public boolean isTooltipContentBlockPresent() {
        return isElementPresent(By.cssSelector(".popover-content"));
    }

    public void enterFiberGroupComment(String comment) {
        enter(comment).into(fiberGroupComment);
    }

    public String getFiberGroupComment() {
        return fiberGroupComment.getAttribute("value");
    }

    public void setStartingFiberNumberValue(String value) {
        startingFiberNumber.clear();
        clickOn(startingFiberNumber);
        enter(value).into(startingFiberNumber);
    }

    public void setNumberOfFibersValue(String value) {
        numberOfFibers.clear();
        clickOn(numberOfFibers);
        enter(value).into(numberOfFibers);
    }

    public void setConnectionsValue(String value) {
        connectionsCount.clear();
        clickOn(connectionsCount);
        enter(value).into(connectionsCount);
    }

    public void setSplicesValue(String value) {
        spliceCount.clear();
        clickOn(spliceCount);
        enter(value).into(spliceCount);
    }

    public void setEnd1NameValue(String value) {
        end1Name.clear();
        enter(value).into(end1Name);
    }

    public void setEnd2NameValue(String value) {
        end2Name.clear();
        enter(value).into(end2Name);
    }

    public void setEndConnectorValueByName(String connectorName, String connectorOption) {
        WebElement connector = find(By.xpath(String.format("//span[text()='%s']/parent::label/following-sibling::div/select", connectorName)));
        Select select = new Select(connector);
        select.selectByVisibleText(connectorOption);
    }

    public void selectEndConnectorCheckBox(String connectorName, String connectorOption) {
        String connectorOptionXpath = "//span[text()='%s']/parent::label/parent::div/following-sibling::div[position()=1]//label[normalize-space()='%s']/input";
        clickOn(find(By.xpath(String.format(connectorOptionXpath, connectorName, connectorOption))));
    }
}
