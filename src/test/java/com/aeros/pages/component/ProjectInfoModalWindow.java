package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import com.aeros.utils.PatternMatcherUtils;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.aeros.constants.Const.RegExp.*;

public class ProjectInfoModalWindow extends WebBasePage {
    public ProjectInfoModalWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "h3.modal-title")
    private WebElement modalWindowTitle;

    public String getProjectInfoTitle() {
        return PatternMatcherUtils.getMatcherString(modalWindowTitle.getText(), TEXT_AFTER_COLON_EXPRESSION, 0).trim();
    }

    public String getProjectInfoValue(String key) {
        WebElement element = find(By.xpath(String.format("//b[text()='%s:']", key)));
        return getNextSiblingElementNodeText(element);
    }

}
