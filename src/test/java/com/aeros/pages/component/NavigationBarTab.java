package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static ch.lambdaj.Lambda.extract;
import static ch.lambdaj.Lambda.on;

public class NavigationBarTab extends WebBasePage {
    public NavigationBarTab(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".username .nav-label")
    private WebElement usernameLabel;

    @FindBy(css = ".header .navbar-nav >li.ng-scope>a span:not(.caret)")
    private List<WebElement> navigationTabs;

    @FindBy(css = ".navbar-nav .dropdown.open .dropdown-menu")
    private WebElement userNameDropDownBlock;

    @FindBy(css = "[href='/logout']")
    private WebElement logoutLink;

    public String getUserNameLabel() {
        return usernameLabel.getText();
    }

    public List<String> getNavigationTabList() {
        return extract((navigationTabs), on(WebElement.class).getText());
    }

    public void openUserNameDropDown() {
        clickOn(usernameLabel);
        waitForElementVisibility(userNameDropDownBlock);
    }

    public void clickOnLogoutLink() {
        clickOn(logoutLink);
    }

    public boolean isNavigationTabActive(String tab) {
        String activeTabXpath = "//span[text()='%s']/parent::a[@class='active']";
        return find(By.xpath(String.format(activeTabXpath, tab))).isDisplayed();
    }

}
