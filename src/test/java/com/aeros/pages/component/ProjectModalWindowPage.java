package com.aeros.pages.component;


import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProjectModalWindowPage extends WebBasePage {
    public ProjectModalWindowPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".modal-content.ng-scope")
    private WebElement modalContentBlock;

    @FindBy(id = "projectName")
    private WebElement projectNameInput;

    @FindBy(id = "projectComment")
    private WebElement projectCommentInput;

    @FindBy(css = ".modal-footer [type='Submit']")
    private WebElement submitProjectButton;

    @FindBy(id = "newProjectName")
    private WebElement newProjectNameField;

    public void clickOnProjectSubmitButton() {
        clickOn(submitProjectButton);
    }

    public void clickOnCreateProjectSubmitButton() {
        clickViaJS(submitProjectButton);
    }

    public void enterProjectName(String projectName) {
        waitForElementVisibility(projectNameInput);
        projectNameInput.clear();
        enter(projectName).into(projectNameInput);
    }

    public String getProjectNameValue() {
        return projectNameInput.getAttribute("value");
    }

    public void enterProjectComment(String projectComment) {
        projectCommentInput.clear();
        enter(projectComment).into(projectCommentInput);
    }

    public void clearProjectCommentField() {
        projectCommentInput.clear();
    }

    public boolean isModalContentBlockDisplayed() {
        return modalContentBlock.isDisplayed();
    }

    public void selectProjectToMerge(String project) {
        clickOn(find(By.xpath(String.format("//table[@id='projectsList']//td[text()='%s']", project))));
    }

    public boolean isProjectForMergingSelected(String project) {
        return isElementPresent(By.xpath(String.format("//table[@id='projectsList']//tr[@class='ng-scope active']//td[text()='%s']", project)));
    }

    public String getSubmitProjectButtonAttribute() {
        System.out.println(submitProjectButton.getAttribute("disabled"));
        return submitProjectButton.getAttribute("disabled");
    }

    public WebElement getNewProjectNameWebElement() {
        return newProjectNameField;
    }


}
