package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class AddCollectionWindow extends WebBasePage {

    public AddCollectionWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "name")
    private WebElement collectionNameField;

    @FindBy(css = ".btn-primary[type='submit']")
    private WebElement saveButton;

    @FindBy(css = "#myModalLabel .fa-pencil")
    private WebElement editCollectionButton;

    public void enterProjectName(String projectName) {
        collectionNameField.clear();
        enter(projectName).into(collectionNameField);
    }

    public boolean isSaveButtonEnabled() {
        return saveButton.isEnabled();
    }

    public void clickOnSaveButton() {
        clickViaJS(saveButton);
    }

    public void clickOnEditCollectionButton() {
        Actions actions = new Actions(getDriver());
        actions.click(editCollectionButton).perform();
    }


}
