package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.WebDriver;

public class ProjectsNavBarComponent extends WebBasePage{
    public ProjectsNavBarComponent(WebDriver driver) {
        super(driver);
    }

    public boolean isProjectNavBarOptionSelected(String option){
        return find(By.xpath(String.format("//ul[@id='project-nav']/li[@class='active']//span[text()='%s']", option))).isDisplayed();

    }
}
