package com.aeros.pages.component;

import com.aeros.pages.WebBasePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestSetupWindow extends WebBasePage {

    public TestSetupWindow(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "testName")
    private WebElement testNameField;

    @FindBy(id = "description")
    private WebElement descriptionField;

    public void checkTestSetupDirectionRadioButton(String value){
        String xpath = "//span[text()='Direction']/parent::label/following-sibling::div//input[@value='%s']";
        clickOn(find(By.xpath(String.format(xpath, value))));
    }

    public void checkTestSetupReferenceRadioButton(String value){
        String xpath = "//span[text()='Reference Method']/parent::label/following-sibling::div/div[normalize-space()='%s']//input";
        clickOn(find(By.xpath(String.format(xpath, value))));
    }

//    public void checkTestSetupReferenceRadioButton(String value){
//        String xpath = "//span[text()='Reference Method']/parent::label/following-sibling::div/div[normalize-space()='%s']//input";
//        clickOn(find(By.xpath(String.format(xpath, value))));
//    }

    //button[text()='Save']


}
