package com.aeros.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonPage extends PageObject {
    public CommonPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".ui-notification.error .message")
    private WebElement errorMessageLabel;

    @FindBy(css = ".ui-notification.success .message")
    private WebElement successfulMessageLabel;

    @FindBy(css = ".ui-notification.success.killed .message")
    private WebElement invisibleSuccessfulMessageLabel;

    public void waitForErrorNotificationMessage() {
        waitForElementVisibility(errorMessageLabel);
    }

    private void waitForElementVisibility(WebElement errorMessageLabel2) {
		
	}

	public void waitForSuccessfulNotificationMessage() {
        waitForElementVisibility(successfulMessageLabel);
    }

    public void waitForSuccessfulNotificationMessageInvisibility() {
        waitForElementVisibility(invisibleSuccessfulMessageLabel);
    }

    public String getErrorNotificationMessage() {
        return errorMessageLabel.getText();
    }

    public String getSuccessfulMessageText() {
        return successfulMessageLabel.getText();
    }

    public void waitForSuccessfulMessageToAppear(String message) {
        waitForElementVisibility(find(By.xpath(String.format("//div[contains(@class,'success')]/div[text()='%s']", message))));
    }

    public boolean isSuccessfulMessageDisplayed(String message) {
        return find(By.xpath(String.format("//div[contains(@class,'success')]/div[text()='%s']", message))).isDisplayed();
    }

    public void waitForMessageAppearance(String message){
        waitForElementVisibility(find(By.xpath(String.format("//div[@class='message ng-binding' and text()='%s']", message))));
    }

    public boolean verifySuccessfulMessageIsDisplayed(String message){
        return find(By.xpath(String.format("//div[@class='message ng-binding' and text()='%s']", message))).isDisplayed();
    }

    public boolean isErrorMessageBlockPresent(){
        return isElementPresent(By.cssSelector(".ui-notification.error .message"));
    }

	private boolean isElementPresent(org.openqa.selenium.By cssSelector) {
		
		return false;
	}

}
