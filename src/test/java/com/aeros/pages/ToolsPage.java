package com.aeros.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.thoughtworks.selenium.webdriven.commands.SelectOption;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;

public class ToolsPage extends WebBasePage {
	
    public ToolsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id='fiberGroupToolbar']/div[1]/ul/li[2]/a")
    private WebElement ToolsTab;
    
    @FindBy(xpath = "//*[@id='fiberGroupToolbar']/div[1]/ul/li[2]/div/select")
    private WebElement toolDropdown;
    
    @FindBy(xpath = "//*[@id='tools-tab']/div/div[1]/div/div[1]/button")
    private WebElement toolAdd; 
    
    @FindBy(xpath = "//*[@id='tools-tab']/div/div[1]/div/div[1]/ul/li[1]/a")
    private WebElement SingleMode; 
    
    @FindBy(xpath = "//*[@id='tools-tab']/div/div[1]/div/div[1]/ul/li[2]/a")
    private WebElement MultiMode;
    
   @FindBy(xpath = "//*[@id='testName']")
    private WebElement SingleModeInspectionTestSetup; 
   
   @FindBy(xpath = "//*[@id='ToolLibForm']/div[2]/div[2]/div/div/input")
   private WebElement SingleModeOTDRTestSetup;
   
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[3]/div/div[1]/label/input")
    private WebElement SelectBothEnds;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[2]/label/input")
    private WebElement SelectIEC45;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[2]/button[2]")
    private WebElement SaveButton;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/div[3]/div/button[2]")
    private WebElement OTDRToolSaveButton;
    
    @FindBy(xpath = "html/body/div[1]/div[1]/div[2]/div/div/div[2]/div/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/div[8]/button[2]")
    private WebElement Inspection_tool_Details;
    
    @FindBy(xpath = "//*[@id='description']")
    private WebElement OLTSDescription;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[8]/div/label[2]/input")
    private WebElement BidirectionalOption;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[9]/div/div[1]/label/input")
    private WebElement OneCordReference;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[3]/div/div[2]/label/input")
    private WebElement SelectOnlyEnd1;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[3]/div/div[3]/label/input")
    private WebElement SelectOnlyEnd2;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[1]/label/input")
    private WebElement SelectNone;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[3]/label/input")
    private WebElement SelectIEC_APC;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[4]/label/input")
    private WebElement SelectIEC_26;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[5]/label/input")
    private WebElement SelectATandT;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[6]/label/input")
    private WebElement SelectIPC;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[2]/label/input")
    private WebElement SelectIECPCMM;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[3]/label/input")
    private WebElement SelectAT_TMM;
    
    @FindBy(xpath = "//*[@id='consumer']/div[1]/div/div/div/form/div[1]/div[4]/div/div[4]/label/input")
    private WebElement SelectIPCMM;
    
    @FindBy(xpath = "//*[@id='page-instructions']")
    private WebElement HelpButton;
    
    @FindBy(xpath = "//*[@id='fiber-groups']/div/div[2]/div/div/div[3]/button[4]")
    private WebElement FiberGroupResultButton;
    
    @FindBy(xpath = "//*[@id='olts-test-results']/div[1]/div/div[2]/div/div/div/div[1]/div[2]/div/div/div[3]/button[1]")
    private WebElement TraceButton;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/div[2]/div[8]/div/afl-modal-carousel-input/ng-switch/select")
    private WebElement selectLaunchCable;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[2]/div/div[1]")
    private WebElement clickDownArrowOfLinkpfCriteria; 
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/div[2]/div[11]/div/afl-modal-carousel-input/ng-switch/afl-modal-carousel-input/ng-switch/afl-modal-carousel-input/ng-switch/input")
    private WebElement selectreceiveCable;
    
    @FindBy(xpath ="//*[@id='ToolLibForm']/div[2]/div[9]/div/afl-modal-carousel-input/ng-switch/afl-modal-carousel-input/ng-switch/afl-modal-carousel-input/ng-switch/input")
    private WebElement launchCableLength;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/div[2]/div[10]/div/afl-modal-carousel-input/ng-switch/select")
    private WebElement receiveCableLength;
    
    @FindBy(xpath= "//*[@id='project-nav']/li[2]/a/span")
    private WebElement ResultButton;
    
    @FindBy(xpath = "//*[@id='main-content']/div[1]/a")
    private WebElement aeRosLogo;
    
    @FindBy(xpath = "//*[@id='main-content']/div[1]/ul/li[1]/a")
    private WebElement OrganizationalMenu1;
    
    @FindBy(xpath = "//*[@id='tools-menu-dropdown']")
    private WebElement OrganizationalMenu2;
    
    @FindBy(xpath = "//*[@id='main-content']/div[1]/ul/li[3]/a") 
    private WebElement OrganizationalMenu3;
    
    @FindBy(xpath = "//*[@id='main-content']/div[1]/ul/li[4]/a/span[1]")
    private WebElement OrganizationalMenu;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[1]/div/div[2]/div[2]/afl-modal-carousel-input/ng-switch/select")
    private WebElement Event_Start_Connector;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[1]/div/div[2]/div[3]/afl-modal-carousel-input/ng-switch/select")
    private WebElement Event_End_Connector;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[2]/div/div[2]/div[2]/afl-modal-carousel-input/ng-switch/select")
    private WebElement Link_Start_Connector;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[2]/div/div[2]/div[3]/afl-modal-carousel-input/ng-switch/select")
    private WebElement Link_End_Connector;
    
    @FindBy(xpath = "//*[@id='ToolLibForm']/afl-modal-carousel-section[1]/div/div[2]")
    private WebElement PopupWindow;
    
  
    
    
    
  /*  @FindBy(xpath = "//*[@id="fiber-groups"]/div/div[2]/div/div/div[3]/button[4]");
    private WebElement FiberGroupResultButton;
    
  //*[@id="fiber-groups"]/div/div[2]/div/div/div[3]/button[4]*/		
    
    public void clickOnToolsTabButton() {
        clickOn(ToolsTab);
    }
    public void scrollUPWindow()
    {
    	
    }
    
    public void selectToolType(String tooltype)
    {
    	 selectFromDropdown(toolDropdown, tooltype);
              
    	    	
    }
    public void enterValueForRC(String option)
    {
    	
    	
    }
    public void selectEventStartConnector(String option)
    {
    	selectFromDropdown(Event_Start_Connector, option);
    }
    public void selectEventEndConnector(String option)
    {
    	selectFromDropdown(Event_End_Connector, option);
    }
    public void selectLinkStartConnector(String option)
    {
    	selectFromDropdown(Link_Start_Connector, option);
    }
    
    public void selectLinkEndConnector(String option)
    {
    	selectFromDropdown(Link_End_Connector, option);
    }
    
    public void selectLaunchCable(String entry)
    {
    	 selectFromDropdown(selectLaunchCable, entry);
    }
    
    public void selectReceiveCable(String entry)
    {
    	 selectFromDropdown(receiveCableLength, entry);
    }
    
    public void clickOnToolsAddButton() {
    	clickOn(toolAdd);
    }
   

    public void aeRosLogoPresent() {
    	aeRosLogo.isDisplayed();
    }
    
    public boolean organizationalMenuPresent()
    {
    	if (OrganizationalMenu.isDisplayed() && OrganizationalMenu3.isDisplayed() && OrganizationalMenu2.isDisplayed() && OrganizationalMenu1.isDisplayed())
    	return true;
    	else
    		return false;
    			
    }
    
    public void clickOnAddSingleMode() {
    	clickOn(SingleMode);
    }
    
    public void clickOnAddMultiMode()
    {
    	clickOn(MultiMode);
    }
   public void addTestName(String toolname) {
    	SingleModeInspectionTestSetup.sendKeys(toolname);
   }
  
   public void addOTDRToolName(String toolname) {
   	SingleModeOTDRTestSetup.sendKeys(toolname);
  }
    
  public void clickOnBothEnds() {
    	clickOn(SelectBothEnds);
    }
  
  public void clickOnOnlyEnd1()
  {
	  clickOn(SelectOnlyEnd1);
  }
  
  public void clickOnlyEnd2()
  {
	  clickOn(SelectOnlyEnd2);
  }
  
  public void enterDescriptionForOLTSTool(String des)
    {
	  OLTSDescription.sendKeys(des);  
  }
    
    public void clickOnIEC45Rule() {
    	clickOn(SelectIEC45);
    }
    
    public void clickOnNonePassFailStandard()
    {
    	clickOn(SelectNone);
    }
    public void clickOnIECAPCRule()
    {
    	clickOn(SelectIEC_APC);
    }
    
    public void clickOnIEC26Rule()
    {
    	clickOn(SelectIEC_26);
    }
    
    public void clickOnATandTRule()
    {
    	clickOn(SelectATandT);
    }
    
    public void clickOnIPCRule()
    {
    	clickOn(SelectIPC);
    }
    
    public void clickOnIECPCMM()
    {
    	clickOn(SelectIECPCMM);
    }
    
    public void clickOnAT_TMM()
    {
    	clickOn(SelectAT_TMM);
     }
    
    public void clickOnIPCMM()
    {
    	clickOn(SelectIPCMM);
    }
    
    public void clickOnHelpButton()
    {
    	clickOn(HelpButton);
    }
    
    public void clickOnLinkPassFailDownArrow()
    {
    	clickDownArrowOfLinkpfCriteria.click();
    }
    
    public void clickOnSaveButton() {
    	clickOn(SaveButton);
    }
    
    public void clickOnOTDRToolSaveButton() {
    	clickOn(OTDRToolSaveButton);
    }
     
    public void selectBidirectionalOption()
    {
    	clickOn(BidirectionalOption);
    }
    
    public void selectOneCordreferenceOption()
    {
    	clickOn(OneCordReference);
    	
    }
    public void clickOnResultButton()
    {
    	FiberGroupResultButton.click();
    }
    
    public void clickOnTraceButton()
    {
    	TraceButton.click();
    }
    public void selectToolByName(String name)
    {
    	//*[text()[contains(.,'Automation_OTDR_Tool')]]
    	 String ToolXpath ="//*[text()[contains(.,\"%s\")]]";
    	 clickOn(find(By.xpath(String.format(ToolXpath, name))));
    }
    
    public void waitForToolVisibility(String name)
    {
    	//*[text()[contains(.,'Automation_OTDR_Tool')]]
    	 String ToolXpath ="//*[text()[contains(.,\"%s\")]]";
         waitForElementVisibility(find(By.xpath(String.format(ToolXpath, name))));
    }
    
    public void enterLCLength(String val)
    {
    	launchCableLength.clear();
    	launchCableLength.sendKeys(val);
    }
    
    public void waitForLCLengthVisibility()
    {
    	//*[text()[contains(.,'Automation_OTDR_Tool')]]
    	waitForElementVisibility(launchCableLength);
    }
    public void enterRCLength(String val)
    {
    	selectreceiveCable.clear();
    	selectreceiveCable.sendKeys(val);
    }
    
    public void waitForRCLengthVisibility()
    {
    	//*[text()[contains(.,'Automation_OTDR_Tool')]]
    	waitForElementVisibility(selectreceiveCable);
    }
    
    public void waitForResultButtonVisibility()
    {
    	//*[text()[contains(.,'Automation_OTDR_Tool')]]
    	waitForElementVisibility(ResultButton);
    }
    
    
    public boolean isToolVisible(String name)
    {
    	String ToolXpath ="//*[text()[contains(.,\"%s\")]]";
    	return isElementPresent(By.xpath(String.format(ToolXpath, name)));
    	//return isElementPresent(By.xpath(String.format(projectCommentXpath, projectName)));
    }
    public void clickOnIbutton() {
    	clickOn(Inspection_tool_Details);
    }
}
