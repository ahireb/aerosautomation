package com.aeros.pages;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.aeros.constants.Const.Timeout.*;


public class WebBasePage extends PageObject {

    private WebDriverWait webDriverWait;

    public WebBasePage(WebDriver driver) {
        super(driver);
        webDriverWait = new WebDriverWait(driver, WEB_DRIVER_WAITING_BY_DEFAULT);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(IMPLICITY_WAIT_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(SCRIPT_TIMEOUT, TimeUnit.SECONDS);
        //driver.manage().window().maximize();
    }

    public String getCurrentPageUrl() {
        return getDriver().getCurrentUrl();
    }
    
    
    public void maximizeWindow() {
        getDriver().manage().window().maximize();
    }

    public void waitForProjectNameTextDisappeared(String projectName) {
        waitForTextToDisappear(projectName);
    }

    public void waitForTextValueToBeAppeared(WebElement element, String text) {
        webDriverWait.until(ExpectedConditions.textToBePresentInElementValue(element, text));
    }

    public void waitForProjectNameTextAppeared(String projectName) {
        waitForTextToAppear(projectName);
    }

    public void waitForElementInvisibility(By elementLocator) {
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(elementLocator));
    }

    public void waitForElementAbscence(By elementLocator) {
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(elementLocator));
    }


    public void waitForElementInvisibility(By elementLocator, String text) {
        webDriverWait.until(ExpectedConditions.invisibilityOfElementWithText(elementLocator, text));
    }

    protected boolean isElementPresent(By locator) {
        getDriver().manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        try {
            return getDriver().findElements(locator).size() > 0;
        } catch (NoSuchElementException ex) {
            return false;
        } finally {
            getDriver().manage().timeouts().implicitlyWait(IMPLICITY_WAIT_TIMEOUT, TimeUnit.SECONDS);
        }
    }

    public void waitForElementVisibility(WebElement element) {
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForElementToBeClickable(WebElement element) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public void waitForElementToBeClickable(By locator) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public String getNextSiblingElementNodeText(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        return (String) executor.executeScript("return arguments[0].nextSibling.textContent.trim()", element);
    }

    public void clickViaActions(WebElement webElement) {
        Actions actions = new Actions(getDriver());
        actions.click(webElement).perform();
    }

    public void clickViaJS(WebElement webElement) {
        JavascriptExecutor executor = (JavascriptExecutor) getDriver();
        executor.executeScript("arguments[0].click();", webElement);
    }
}
