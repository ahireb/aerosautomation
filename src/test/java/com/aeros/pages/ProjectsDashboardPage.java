package com.aeros.pages;

import com.aeros.pages.component.*;

import net.serenitybdd.core.annotations.findby.FindBy;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProjectsDashboardPage extends WebBasePage {

	
	@FindBy(xpath = "html/body/div[1]/div[1]/div[1]/h2/span[1]")
	private WebElement ProjectList;
	@FindBy(xpath = "html/body/div[1]/div/div/div[1]/h3")
	private WebElement HelpPage;
	@FindBy(xpath = "html/body/div[1]/div/div/div[1]/i")
	private WebElement CloseHelpPage;
	@FindBy(xpath = "html/body/div[1]/div[1]/div[2]/div/div/div[1]/div[2]/select")
	private WebElement DropDownList;
	@FindBy(xpath = "html/body/div[1]/div[1]/div[2]/div/div/div[1]/progress-bar/div/div")
	private WebElement ProjectProgressbar;
	
	
	
    public ProjectsDashboardPage(WebDriver driver) {
    	
        super(driver);
        
    }

    public HeaderComponent getHeaderTab(){
        return new HeaderComponent(getDriver());
    }

    public ProjectFilterTabComponent getProjectsFilterTab(){
        return new ProjectFilterTabComponent(getDriver());
    }

    public ProjectModalWindowPage getProjectsModalWindow(){
        return new ProjectModalWindowPage(getDriver());
    }

    public ModalWindowPage getModalWindow(){
        return new ModalWindowPage(getDriver());
    }

    public ProjectTableComponent getProjectTableComponent(){
        return new ProjectTableComponent(getDriver());
    }


    public ProjectInfoModalWindow getProjectInfoModalWindow(){
        return new ProjectInfoModalWindow(getDriver());
    }
    
    public void verifyProjectListPresent()
    {
    	ProjectList.isDisplayed();
    }
    
    public void verifyHelpPagePresent()
    {
    	HelpPage.isDisplayed();
    }
    public void toCloseHelpPage()
    {
    	CloseHelpPage.click();
    }
    public void clickOnActiveFromDropDownList(String option)
    {
    	selectFromDropdown(DropDownList, option);
    }
    public void checkProgressbar()
    {
    	ProjectProgressbar.isEnabled();
    }
    
    

}