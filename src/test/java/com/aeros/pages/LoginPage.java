package com.aeros.pages;

import com.aeros.model.User;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.findby.By;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@DefaultUrl("https://afl-uat.appspot.com/login")
public class LoginPage extends WebBasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }
    private CommonPage commonPage;
    private ProjectsDashboardPage dashboardPage;

    @FindBy(id="email")
    private WebElement usernameInput;

    @FindBy(id = "password")
    private WebElement passwordInput;

    @FindBy(css = ".btn-primary[type='submit']")
    private WebElement signInButton;

    @FindBy(css = ".btn-default")
    private WebElement forgotPasswordButton;

    @FindBy(id = "forcedLogin")
    private WebElement forceLoginButton;

    @FindBy(css = ".form-signin")
    private WebElement authorizationForm;

    public void enterUsername(String username) {
    
    	/*
    	
    	JavascriptExecutor executor = (JavascriptExecutor) getDriver();
    	
    	String uname =  "arguments[0].value = \'" + username +  "\'";
    	
    	executor.executeScript(uname, usernameInput);
    	
    	*/
    	
    	
    
    
    
    
        usernameInput.clear();
      
        enter(username).into(usernameInput);
    
    }

    public void enterPassword(String password) {  	
  	    	    	
        passwordInput.clear();
        enter(password).into(passwordInput);
        
    }

    public void signIntoApp(){
        clickOn(signInButton);
    }


    public void reAuthorizeIntoApp(User user){
        if(commonPage.isErrorMessageBlockPresent() && commonPage.getErrorNotificationMessage().equals("User is already logged in")){
            clickOn(forceLoginButton);
            enterUsername(user.getUserEmail());
            enterPassword(user.getUserPassword());
            signIntoApp();
        }
    }

    public void updateSession(User user) {
        if(commonPage.isErrorMessageBlockPresent() && commonPage.getErrorNotificationMessage().equals("User is already logged in")){
            clickOn(forceLoginButton);
            enterUsername(user.getUserEmail());
            enterPassword(user.getUserPassword());
            signIntoApp();
            dashboardPage.getHeaderTab().getNavigationTab().openUserNameDropDown();
            dashboardPage.getHeaderTab().getNavigationTab().clickOnLogoutLink();
            waitForAuthorizationFormVisibility();
        }

    }

    public void waitForAuthorizationFormVisibility(){
        waitForElementVisibility(authorizationForm);
    }
}