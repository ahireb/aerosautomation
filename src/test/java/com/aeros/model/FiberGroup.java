package com.aeros.model;

public class FiberGroup {

    private String fiberGroupName;
    private String fiberType;
    private String startingFiberNumber;
    private String numberOfFibers;
    private String numberOfConnections;
    private String numberOfSplices;
    private String end1Name;
    private String end2Name;
    private String end1Connector;
    private String end2Connector;
    private String comment;
    private String end1ConnectorOption;
    private String end2ConnectorOption;

    private FiberGroup() {
    }

    public String getFiberGroupName() {
        return fiberGroupName;
    }

    public String getFiberType() {
        return fiberType;
    }

    public String getStartingFiberNumber() {
        return startingFiberNumber;
    }

    public String getNumberOfFibers() {
        return numberOfFibers;
    }

    public String getComment() {
        return comment;
    }

    public String getNumberOfSplicers() {
        return numberOfSplices;
    }

    public String getNumberOfConnections() {
        return numberOfConnections;
    }

    public String getEnd1Name() {
        return end1Name;
    }

    public String getEnd2Name() {
        return end2Name;
    }

    public String getEnd1Connector() {
        return end1Connector;
    }

    public String getEnd1ConnectorOption() {
        return end1ConnectorOption;
    }

    public String getEnd2Connector() {
        return end2Connector;
    }

    public String getEnd2ConnectorOption() {
        return end2ConnectorOption;
    }

    public static FiberGroup.Builder builder(String fiberGroupName) {
        return new FiberGroup().new Builder(fiberGroupName);
    }

    public class Builder {

        private Builder(String fiberGroupName) {
            FiberGroup.this.fiberGroupName = fiberGroupName;
            FiberGroup.this.fiberType = "All SMF";
            FiberGroup.this.startingFiberNumber = "1";
            FiberGroup.this.numberOfFibers = "12";
            FiberGroup.this.numberOfConnections = "2";
            FiberGroup.this.numberOfSplices = "0";
            FiberGroup.this.end1Name = "End1";
            FiberGroup.this.end1Connector = "None";
            FiberGroup.this.end2Name = "End2";
            FiberGroup.this.end2Connector = "None";
            FiberGroup.this.comment = "test comment";
        }

        public Builder setFiberGroupName(String fiberGroupName){
            FiberGroup.this.fiberGroupName = fiberGroupName;
            return this;
        }

        public Builder setNumberOfConnections(String numberOfConnections){
            FiberGroup.this.numberOfConnections = numberOfConnections;
            return this;
        }

        public Builder setNumberOfSplices(String numberOfSplices){
            FiberGroup.this.numberOfSplices = numberOfSplices;
            return this;
        }

        public Builder setFiberType(String fiberType){
            FiberGroup.this.fiberType = fiberType;
            return this;
        }

        public Builder setEnd1Name(String end1Name){
            FiberGroup.this.end1Name = end1Name;
            return this;
        }

        public Builder setEnd2Name(String end2Name){
            FiberGroup.this.end2Name = end2Name;
            return this;
        }

        public Builder setEnd1Connector(String end1Connector){
            FiberGroup.this.end1Connector = end1Connector;
            return this;
        }

        public Builder setEnd2Connector(String end2Connector){
            FiberGroup.this.end2Connector = end2Connector;
            return this;
        }

        public Builder setStartingFiberNumber(String startingFiberNumber){
            FiberGroup.this.startingFiberNumber = startingFiberNumber;
            return this;
        }

        public Builder setNumberOfFibers(String numberOfFibers){
            FiberGroup.this.numberOfFibers = numberOfFibers;
            return this;
        }

        public Builder setComment(String comment){
            FiberGroup.this.comment = comment;
            return this;
        }

        public Builder setEnd1ConnectorOption(String end1ConnectorOption){
            FiberGroup.this.end1ConnectorOption = end1ConnectorOption;
            return this;
        }

        public Builder setEnd2ConnectorOption(String end2ConnectorOption){
            FiberGroup.this.end2ConnectorOption = end2ConnectorOption;
            return this;
        }

        public FiberGroup build() {
            return FiberGroup.this;
        }

    }

}

