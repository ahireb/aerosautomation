 package com.aeros.model;

import com.aeros.utils.PropertyLoader;

public class User {

    private String email;
    private String password;
    private String role;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String role) {
        this.role = role;
    }

    public User() {
    }

    public String getUserEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public User loadCredentials(String email, String password) {
        this.email = PropertyLoader.getProperty(role + "." + email);
        this.password = PropertyLoader.getProperty(role + "." + password);
        return this;
    }

}
