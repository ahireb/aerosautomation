package com.aeros.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ProjectModel {
	
	String filename;
	
	
	public ProjectModel(String projectfile)
	{
		filename = projectfile;
	}
	
	public List<String> readProjectFile()
	{
	  List<String> records = new ArrayList<String>();
	  try
	  {
	    BufferedReader reader = new BufferedReader(new FileReader(filename));
	    String line;
	    while ((line = reader.readLine()) != null)
	    {
	      records.add(line);
	    }
	    reader.close();
	    return records;
	  }
	  catch (Exception e)
	  {
	    System.err.format("Exception occurred trying to read '%s'.", filename);
	    e.printStackTrace();
	    return null;
	  }
	}
	
	public String getProjectName(String line)
	{
		
			
		String[] tokens = line.split(",");       // Single comma is the separator.
		
		for (int i = 0; i < tokens.length; i++) {           
	        //Do your stuff here
			String[] sub_tokens = line.split(":");       // Single colon is the separator.
			if( sub_tokens[0] == "Name")
			{
				return sub_tokens[1];
			}
	        
	    }
		
		return null;
	}

	
	public String getProjectComment(String line)
	{
		
			
		String[] tokens = line.split(",");       // Single comma is the separator.
		
		for (int i = 0; i < tokens.length; i++) {           
	        //Do your stuff here
			String[] sub_tokens = line.split(":");       // Single colon is the separator.
			if( sub_tokens[0] == "Comment")
			{
				return sub_tokens[1];
			}
	        
	    }
		
		return null;
	}

	
}
