package com.aeros.model;

import com.aeros.utils.DateUtils;

import static com.aeros.constants.Const.DateFormat.*;

public class Project {

    private String projectName;
    private String comment;
    private String currentDate;
    private String projectCreator;
    private String projectStatus;

    public Project(String projectName, String comment){
        this.projectName = projectName;
        this.comment = comment;
        this.currentDate = DateUtils.getCurrentDateTime(SLASH_FORMAT);
    }

    public Project(String projectName){
        this.projectName = projectName;
        this.currentDate = DateUtils.getCurrentDateTime(SLASH_FORMAT);
    }
    
    
    

    public Project(){
    	
    	//Read from Excel sheet	
    }


    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getProjectStatus() {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus) {
        this.projectStatus = projectStatus;
    }

    public String getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(String projectCreator) {
        this.projectCreator = projectCreator;
    }
}
