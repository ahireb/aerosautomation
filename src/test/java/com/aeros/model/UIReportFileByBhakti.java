package com.aeros.model;
import java.io.*;


public class UIReportFileByBhakti {
	
	 FileReader fileReader;
	 BufferedReader bufferedReader;
	 
	public UIReportFileByBhakti(String filename) 
	{

		try {
			fileReader = new FileReader(filename);
			bufferedReader = new BufferedReader(fileReader);
		
			}   
		catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                filename + "'");                
        	}
	}
	public String getNextLine()
	{
		String line = null;
		
		try{
			line= bufferedReader.readLine();
			}
		catch (IOException ex){
			System.out.println("Unable to read file");
		}
		if(line.trim().length() == 0)
			return null;
		
		return line;
				
		}
	
	public String getStepName(String line)
	{
		String [] parts = line.split(":");
		System.out.println("Parts 0 := " +parts[0]);
		System.out.println("Parts 1 := " +parts[1]);
		if( parts[0].trim().equals("STEP_TITLE"))
			return parts[1];
		
		return null;
	}
	
	public String getStepInfo(String line)
	{
		String [] parts = line.split(":");
		System.out.println("Parts 0 := " +parts[0]);
		System.out.println("Parts 1 := " +parts[1]);
		if( parts[0].trim().equals("STEP_INFO"))
			return parts[1];
		
		return null;
	}	
		
	public String getStepResult(String line)
	{
		String [] parts = line.split(":");
		System.out.println("Parts 0 := " +parts[0]);
		System.out.println("Parts 1 := " +parts[1]);
		if( parts[0].trim().equals("STEP_RESULT"))
			return parts[1];
		
		return null;
	}	
	public String getStepFailure(String line)
	{
		String [] parts = line.split(":");
		System.out.println("Parts 0 := " +parts[0]);
		System.out.println("Parts 1 := " +parts[1]);
		if( parts[0].trim().equals("STEP_FAILURE"))
			return parts[1];
		
		return null;
	}
		
		
}