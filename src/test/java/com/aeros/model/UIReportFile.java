package com.aeros.model;
import java.io.*;

public class UIReportFile {
	
	String filename;
    FileReader fileReader;
    BufferedReader bufferedReader;
	

	
	public UIReportFile(String file)
	{
		   try {
	            // FileReader reads text files in the default encoding.
	             fileReader = new FileReader(file);

	            // Always wrap FileReader in BufferedReader.
	            bufferedReader =  new BufferedReader(fileReader);

//	            while((line = bufferedReader.readLine()) != null) {
	//                System.out.println(line);
	            }
	            
	            catch(FileNotFoundException ex) {
	                System.out.println(
	                    "Unable to open file\n '" + 
	                    file + "'");                
	            }

	}
	
	public String getNextLine()
	{
		String line = null;
		
		try {
			line = bufferedReader.readLine();
		}
		catch (IOException ex)
		{
			System.out.println("Unable to read UI report file\n");
		}
		
		if (line.trim().length() == 0)
			return null;
			
		return line;
			
	}
	
	public String getStepTitle(String line)
	{
		String[] parts = line.split(":");
		
		System.out.println("Part 0 := " + parts[0]);
		System.out.println("Part 1 := " + parts[1]);
		if( parts[0].trim().equals("STEP_TITLE"))
			return parts[1];
		
		return null;
	}

	
	public String getStepInfo(String line)
	{
		String[] parts = line.split(":");
		System.out.println("Part 0 := " + parts[0]);
		System.out.println("Part 1 := " + parts[1]);
		
		if( parts[0].trim().equals("STEP_INFO"))
			return parts[1];
		
		return null;
	}
	
	public String getStepResult(String line)
	{
		String[] parts = line.split(":");
		System.out.println("Part 0 := " + parts[0]);
		System.out.println("Part 1 := " + parts[1]);
		
		if( parts[0].trim().equals("STEP_RESULT"))
			return parts[1];
		
		return null;
	}
	
	public String getStepResultFailureInfo(String line)
	{
		String[] parts = line.split(":");
		System.out.println("Part 0 := " + parts[0]);
		System.out.println("Part 1 := " + parts[1]);
		
		if( parts[0].trim().equals("STEP_FAILURE"))
			return parts[1];
		
		return null;
	}
	
}
