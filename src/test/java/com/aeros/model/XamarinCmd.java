package com.aeros.model;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class XamarinCmd 
{ 
	String nunit3Path;
	String DLLPath;
	
	public XamarinCmd(String nunit_path, String dll_path)
	{
		nunit3Path = nunit_path;
		DLLPath = dll_path;
	}
     

	public void execXamarinTest(String test)
    {
		//Prepare parameter list
				
		String test_cmd = "--where name=" + test;
		String finalCmd = nunit3Path + "  " + DLLPath + " "  + test_cmd;
		System.out.println("Xamarin test command prepared := "  + finalCmd + "\n");
		
        try 
        { 
            Process p=Runtime.getRuntime().exec(finalCmd); 
            p.waitFor(); 
            BufferedReader reader=new BufferedReader(
                new InputStreamReader(p.getInputStream())
            ); 
            String line; 
            while((line = reader.readLine()) != null) 
            { 
                System.out.println(line);
            } 

        }
        catch(IOException e1) {e1.printStackTrace();} 
        catch(InterruptedException e2) {e2.printStackTrace();} 

        System.out.println("Done"); 
    } 
  

	public void execXamarinTest(String test, String p1)
    {
		//Prepare parameter list
				
		String test_cmd = "--where name=" + test + "   -p=" + p1;
		String finalCmd = nunit3Path + "  " + DLLPath + " "  + test_cmd;
		System.out.println("Xamarin test command prepared := "  + finalCmd + "\n");
		
        try 
        { 
            Process p=Runtime.getRuntime().exec(finalCmd); 
            p.waitFor(); 
            BufferedReader reader=new BufferedReader(
                new InputStreamReader(p.getInputStream())
            ); 
            String line; 
            while((line = reader.readLine()) != null) 
            { 
                System.out.println(line);
            } 

        }
        catch(IOException e1) {e1.printStackTrace();} 
        catch(InterruptedException e2) {e2.printStackTrace();} 

        System.out.println("Done"); 
    } 
  

}