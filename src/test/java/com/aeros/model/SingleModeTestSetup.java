package com.aeros.model;


public class SingleModeTestSetup {

    private SingleModeTestSetup() {
    }

    private String singleModeType;
    private String testName;
    private String description;
    private String runOLTSTest;
    private String oLTSTestLimit;
    private String runORLTest;
    private String oRLTestLimit;
    private String direction;
    private String referenceMethod;

    public String getTestName() {
        return testName;
    }

    public String getDescription() {
        return description;
    }

    public String getRunOLTSTest() {
        return runOLTSTest;
    }

    public String getoLTSTestLimit() {
        return oLTSTestLimit;
    }

    public String getRunORLTest() {
        return runORLTest;
    }

    public String getoRLTestLimit() {
        return oRLTestLimit;
    }

    public String getDirection() {
        return direction;
    }

    public String getReferenceMethod() {
        return referenceMethod;
    }

    public static SingleModeTestSetup.Builder builder(String singleModeType) {
        return new SingleModeTestSetup().new Builder(singleModeType);
    }

    public class Builder {

        private Builder(String singleModeType) {
            SingleModeTestSetup.this.singleModeType = singleModeType;
        }

        public SingleModeTestSetup.Builder setTestName(String testName) {
            SingleModeTestSetup.this.testName = testName;
            return this;
        }

        public SingleModeTestSetup.Builder setDescription(String description) {
            SingleModeTestSetup.this.description = description;
            return this;
        }

        public SingleModeTestSetup.Builder setRunOLTSTest(String runOLTSTest) {
            SingleModeTestSetup.this.runOLTSTest = runOLTSTest;
            return this;
        }

        public SingleModeTestSetup.Builder setOLTSTestLimit(String oLTSTestLimit) {
            SingleModeTestSetup.this.oLTSTestLimit = oLTSTestLimit;
            return this;
        }


        public SingleModeTestSetup.Builder setRunORLTest(String runORLTest) {
            SingleModeTestSetup.this.runORLTest = runORLTest;
            return this;
        }

        public SingleModeTestSetup.Builder setORLTestLimit(String oRLTestLimit) {
            SingleModeTestSetup.this.oRLTestLimit = oRLTestLimit;
            return this;
        }

        public SingleModeTestSetup.Builder setDirection(String direction) {
            SingleModeTestSetup.this.direction = direction;
            return this;
        }

        public SingleModeTestSetup.Builder setReferenceMethod(String referenceMethod) {
            SingleModeTestSetup.this.referenceMethod = referenceMethod;
            return this;
        }

        public SingleModeTestSetup build() {
            return SingleModeTestSetup.this;
        }
    }
}