package com.aeros.services.restful.api;

import com.aeros.services.restful.api.AerosRestClient;
import com.aeros.services.restful.api.AerosRestService;
import com.aeros.services.restful.api.converter.AdvancedTypeConverter;
import com.aeros.services.restful.api.model.*;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.provider.json.JSONProvider;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.jaxrs.provider.json.utils.*;

//import org.apache.cxf.jaxrs.provider.AbstractConfigurableProvider;

import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Map;

public class RestServiceImpl implements AerosRestService {
    private AerosRestClient proxyClient;
    private MultivaluedMap<String, Object> mapHeaders;
    private WebClient webClient;
    private Response response;

    public RestServiceImpl(String url) {
        setup(url);
    }

    private void setup(String url) {
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        HTTPConduit httpConduit;

        proxyClient = JAXRSClientFactory.create(url, AerosRestClient.class,
                Collections.singletonList(getJSONProvider()));

        httpConduit = (HTTPConduit) WebClient.getConfig(proxyClient).getConduit();
        httpClientPolicy.setAllowChunking(false);
        httpConduit.setClient(httpClientPolicy);

        TLSClientParameters params = httpConduit.getTlsClientParameters();

        if (params == null) {
            params = new TLSClientParameters();
        }

        params.setTrustManagers(new TrustManager[]{new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }});

        params.setDisableCNCheck(true);

        httpConduit.setTlsClientParameters(params);

        webClient = WebClient.fromClient(WebClient.client(proxyClient));
        proxyClient = JAXRSClientFactory.fromClient(webClient, AerosRestClient.class, true);
    }

    private JSONProvider getJSONProvider() {
        JSONProvider jsonProvider = new JSONProvider();
        jsonProvider.setSupportUnwrapped(true);
        jsonProvider.setDropRootElement(true);
        jsonProvider.setDropCollectionWrapperElement(true);
        jsonProvider.setIgnoreNamespaces(true);
        //jsonProvider.setTypeConverter(new AdvancedTypeConverter());
        jsonProvider.setTypeConverter(new AdvancedTypeConverter());
        
        return jsonProvider;
    }

    private void setAutoRedirect(boolean state) {
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        webClient = WebClient.fromClient(WebClient.client(proxyClient));
        HTTPConduit httpConduit = (HTTPConduit) WebClient.getConfig(proxyClient).getConduit();
        httpClientPolicy.setAutoRedirect(state);
        httpConduit.setClient(httpClientPolicy);
    }

    private String getCookieValue(Response response, String key) {
        Map<String, NewCookie> cookies = response.getCookies();
        return cookies.get(key).toCookie().getValue();
    }

    @Override
    public Response login(String email, String password) {
        setAutoRedirect(false);
        Response response = proxyClient.login(email, password);
        setAutoRedirect(true);
        webClient = WebClient.fromClient(WebClient.client(proxyClient));
        Cookie userCookie = new Cookie("__user", getCookieValue(response, "__user"));
        Cookie userAuthCookie = new Cookie("__userAuth", getCookieValue(response, "__userAuth"));
        webClient.cookie(userCookie);
        webClient.cookie(userAuthCookie);
        proxyClient = JAXRSClientFactory.fromClient(webClient, AerosRestClient.class, true);
        return response;
    }

    @Override
    public Organizations organizations() {
        Response response = proxyClient.organization();
        return response.readEntity(Organizations.class);
    }

    @Override
    public Projects projects(String organizationId) {
        Response response = proxyClient.projects(organizationId);
        return response.readEntity(Projects.class);
    }

    @Override
    public Response deleteOrganizationProjects(String organizationId) {
        Project[] projectArray = projects(organizationId).getProjects();
        for (Project project : projectArray) {
            proxyClient.deleteProject(organizationId, project.getId());
        }
        return response;
    }

    public Organization getUserOrganization(String organizationName) {
        for (Organization organization : organizations().getOrganization()) {
            if (organization.getName().equals(organizationName)) {
                return organization;
            }
        }
        return null;
    }

}
