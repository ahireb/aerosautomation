package com.aeros.services.restful.api.converter;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.mapped.DefaultConverter;

public class AdvancedTypeConverter extends DefaultConverter {

    @Override
    public void setEnforce32BitInt(boolean enforce32BitInt) {
        super.setEnforce32BitInt(enforce32BitInt);
    }

    @Override
    public Object convertToJSONPrimitive(String text) {
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return super.convertToJSONPrimitive(text);
    }
}