package com.aeros.services.restful.api;

import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.interceptor.OutInterceptors;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@InInterceptors(interceptors = "org.apache.cxf.interceptor.LoggingInInterceptor")
@OutInterceptors(interceptors = "org.apache.cxf.interceptor.LoggingOutInterceptor")
public interface AerosRestClient {

    @POST
    @Path("/login")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces(MediaType.APPLICATION_FORM_URLENCODED)
    Response login(@QueryParam("email") String email,
                   @QueryParam("password") String password);

    @GET
    @Path("/login")
    Response login();

    @GET
    @Path("/api/roles")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.WILDCARD})
    @Produces(MediaType.APPLICATION_JSON)
    Response roles();

    @GET
    @Path("/organization/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response organization(@PathParam("id") String id);

    @GET
    @Path("/organization/{id}/projects")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response projects(@PathParam("id") String id);

    @DELETE
    @Path("/organization/{id}/projects/{projectId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response deleteProject(@PathParam("id") String id,
                           @PathParam("projectId") String projectId);

    @GET
    @Path("/organization/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response organization();
}
