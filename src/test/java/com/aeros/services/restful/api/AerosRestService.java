package com.aeros.services.restful.api;


import com.aeros.services.restful.api.model.*;

import javax.ws.rs.core.Response;

public interface AerosRestService {
    Response login(String email, String password);

    Organizations organizations();

    Projects projects(String organizationId);

    Response deleteOrganizationProjects(String organizationId);



}
