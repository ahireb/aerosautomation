package com.aeros.services.restful.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class OrganizationIdResponse {

    @XmlElement(name = "id")
    private String id;

    @XmlElement(name = "name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
