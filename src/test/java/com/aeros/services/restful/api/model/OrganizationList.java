package com.aeros.services.restful.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationList {

    @XmlElement(name = "organizations")
    private OrganizationIdResponse organizationList;

    public OrganizationIdResponse getOrganizationObject() {
        return organizationList;
    }

}
