package com.aeros.services.restful.api;


import com.aeros.model.User;
import com.aeros.services.restful.api.RestServiceImpl;
import com.aeros.services.restful.api.model.*;

import static com.aeros.constants.Const.Url.AUTO_AFL_URL;

public class RestActions {
    private User adminUser = new User("admin").loadCredentials("valid.email", "valid.password");

    public void deleteProjects() {
        RestServiceImpl restService = new RestServiceImpl(AUTO_AFL_URL);
        restService.login((adminUser.getUserEmail()), adminUser.getUserPassword());
        //Organization organization = restService.getUserOrganization("DefaultAutomationOrg");
        Organization organization = restService.getUserOrganization("Bhakti QA");
        restService.deleteOrganizationProjects(organization.getId());
    }
}
