package com.aeros.services.restful.api.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Organizations {

    @XmlElement(name = "organizations")
    private Organization[] organization;

    public Organization[] getOrganization() {
        return organization;
    }

}
