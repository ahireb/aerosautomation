package com.aeros.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    private static  Date tmpDate;

    public static String getCurrentDateTime(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date());
    }

    public static String getNewCustomDate(String date, String fromFormat, String toFormat) {
        SimpleDateFormat formatFrom = new SimpleDateFormat(fromFormat);
        SimpleDateFormat newFormat = new SimpleDateFormat(toFormat);
        try {
            tmpDate = formatFrom.parse(date);
        } catch (ParseException exception) {
            exception.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(tmpDate);

        return newFormat.format(c.getTime());
    }

}
