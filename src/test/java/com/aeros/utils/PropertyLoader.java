package com.aeros.utils;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.aeros.constants.Const.FilePath.*;


public class PropertyLoader {

    public static String getProperty(String key) {
        try (InputStream inputStream = new FileInputStream(APPLICATION_PROPERTIES_PATH)) {
            Properties properties = new Properties();
            properties.load(inputStream);

            for (String property : properties.stringPropertyNames()) {
                if (property.equals(key)) {
                    return properties.getProperty(property);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
