package com.aeros.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherUtils {

    public static String getMatcherString(String expression, String pattern, int index) {
        StringBuilder builder = new StringBuilder();
        Matcher matcher = Pattern.compile(pattern).matcher(expression);
        if (matcher.find()){
            builder.append(matcher.group(index));
        }
        return builder.toString();
    }
}
