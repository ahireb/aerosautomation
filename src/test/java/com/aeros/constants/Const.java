package com.aeros.constants;

public abstract class Const {


    private Const() {
    }

    public static class ProjectNavBar {
        public final static String FIBERGROUPS = "Fibergroups";
        public static final String RESULTS = "Results";
        public static final String PROJECT_CONTACTS = "Project Contacts";
        public static final String REPORTS = "Reports";
        public static final String PROJECT_TEAM = "Project Team";
        public static final String Result_Button_XPATH = "html/body/div[1]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[3]/div/div/div[3]/button[4]";
    }

    public static class Dashboard {
        public static final String PROJECTS_TAB = "Projects";
        public static final String TEST_SETUPS_TAB = "Test Setups";
        public static final String TEST_LIMITS_TAB = "Test Limits";
        public static final String ORGANIZATION_TAB = "Organization";

    }

    public static class DateFormat {
        public static final String SLASH_FORMAT = "M/d/yy";
        public static final String FULL_FORMAT = "ddMMyyyyHHmmss";
        public static final String PROJECT_INFO_DATE_FORMAT = "MMM dd, yyyy";

    }

    public static class Timeout {
        public static final int WEB_DRIVER_WAITING_BY_DEFAULT = 15;
        public static final int PAGE_LOAD_TIMEOUT = 300;
        public static final int IMPLICITY_WAIT_TIMEOUT = 15;
        public static final int SCRIPT_TIMEOUT = 10;
    }

    public static class Message {
        public static final String PROJECT_ALREADY_EXISTS = "Project with name '%s' already exists.";
    }

    public static class RegExp {
        public static final String TEXT_INSIDE_BRACKETS_EXPRESSION = "\\[(.*?)\\]";
        public static final int FIRST_GROUP_INDEX = 1;
        public static final String TEXT_INSIDE_QUOTATIONS_EXPRESSION = "\\'(.*?)\\'";
        public static final String TEXT_AFTER_COLON_EXPRESSION = "(?<=:)[^\\]]+";
    }

    public static class Url {
        //public static final String AUTO_AFL_URL = "https://afl-automation.appspot.com";
        public static final String AUTO_AFL_URL = "https://afl-int.appspot.com";
        //public static final String AUTO_AFL_DEFAULT_URL = "https://afl-automation.appspot.com/login";
        public static final String AUTO_AFL_DEFAULT_URL = "https://afl-int.appspot.com/login";
    }

    public static class ModalWindowOperations {
        public static final String CONFIRM_DELETE_BUTTON = "Confirm Delete";
        public static final String CONFIRM_HIDE_BUTTON = "Confirm Hide";
        public static final String CONFIRM_SHOW_BUTTON = "Confirm Show";
    }

    public static class Actions {
        public static final String HIDE_ACTION = "hide";
        public static final String DELETE_ACTION = "delete";
        public static final String RELOAD_ACTION = "reload";
        public static final String DETAILS_ACTION = "details";
        public static final String EDIT_ACTION = "edit";
        public static final String COPY_ACTION = "copy";
        public static final String ADD_ACTION = "add";
        public static final String UPLOAD_ACTION = "upload";
    }

    public static class FilePath {
        public static final String APPLICATION_PROPERTIES_PATH = "src/test/resources/application.properties";
        public static final String NUNIT_TOOL_PATH = "C:\\saturn_automation\\afl-saturn-app\\AFL.Saturn\\packages\\NUnit.ConsoleRunner.3.8.0\\tools\\nunit3-console";
        public static final String XAMARIN_DLL_PATH = "C:\\saturn_automation\\afl-saturn-app\\AFL.Saturn\\AFL.Saturn.UITests\\bin\\Debug\\AFL.Saturn.UITests.dll";
        public static final String UI_REPORT_FILE_PATH = "C:\\saturn_automation\\afl-saturn-app\\AFL.Saturn\\AFL.Saturn.UITests\\bin\\Debug\\ui_report.dat";
    }

    public static class DropDownOptions {
        public static final String HIDDEN_OPTION = "hidden";
        public static final String ACTIVE_OPTION = "active";
    }

    public static class MessageConstants {
        public static final String INVALID_CREDENTIALS = "Invalid Credentials";
        //public static final String FIBER_GROUP_ADDED = "Added '%s'";
        public static final String FIBER_GROUP_ADDED = "Fiber group '%s' added";
        
        public static final String FIBER_GROUP_ALREADY_EXISTS = "Fiber group '%s' already exists";
        public static final String FIBER_GROUP_UPDATED = "Updated '%s'";
        public static final String FIBER_GROUPS_REMOVED = "Removed fiber groups";
        public static final String FILE_COLLECTION_CREATED = "Successfully create file collection.";
        public static final String FILE_COLLECTION_DELETED = "Successfully deleted the file collection.";
        public static final String FILE_COLLECTION_UPDATED = "Successfully Updated collection";
        public static final String FIBER_GROUP_ADDING_ERROR = "Error adding '%s'";
    }

    public static class ModalWindow {
        public static final String EDIT_PROJECT_WINDOW = "Edit project";
        public static final String EDIT_FIBER_GROUP_WINDOW = "Edit fiber group name";
        public static final String DELETE_FIBER_GROUPS_WINDOW = "Delete Selected Fiber Groups?";
        public static final String FIBER_GROUP_NAME_WINDOW = "Fiber Group Name: %s";
        public static final String ADD_COLLECTION_WINDOW = "Add Collection";
        public static final String COLLECTION_WINDOW = "Collection: %s";
        public static final String ATTACHED_TO_A_PROJECT = "Attached to Project: %s";
        public static final String ATTACHED_TO_A_FIBER_GROUP = "Attached to Fibergroup: %s";
        public static final String DELETE_FILE_COLLECTION = "Delete File Collection?";
    }

    public static class DefaultValues {
        public static final String FIBER_GROUP_MEDIA_TYPE_NAME = "%s Fiber group";
    }

    public static class Tooltip {
        public static final String EXPAND_AREA = "expand to add multiple fibergroups (one per line)";
    }
    
    public static class ToolSetup {
   
    	public static final String SELECT_TOOLS_XPATH = "html/body/div[1]/div[1]/div[2]/div/div/div[2]/div/div[1]/div[1]/ul/li[2]/a";		
     }
    
  /*  public static class ResultButton {
    	public static final String Result_Button_XPATH = "html/body/div[1]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[3]/div/div/div[3]/button[4]";
    }*/
   
    public static class DataModel {
    	
    	/*TODO: Hard coded path will be removed later on*/
    	public static final String PROJECT_MODEL_FILE = "C:\\Selenium\\aerosautomation\\data_model";
    }

}
