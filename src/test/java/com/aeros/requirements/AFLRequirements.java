package com.aeros.requirements;

import net.thucydides.core.annotations.Feature;

public class AFLRequirements {

    @Feature
    public class Authorization {
    }

    @Feature
    public class FiberGroups {
    }

    @Feature
    public class ProjectsBaseFunctionality {
    }

}
