package com.aeros.features.project;
import static com.aeros.constants.Const.FilePath.*;

import com.aeros.services.restful.api.RestActions;
import org.junit.Ignore;
import com.aeros.model.Project;
import com.aeros.requirements.*;
import com.aeros.model.User;
//import com.testautomation.afl.servises.restful.api.actions.RestActions;
//import com.testautomation.afl.stories.requirements.AFLRequirements;
import com.aeros.steps.CommonSteps;
import com.aeros.steps.ProjectSteps;
import com.aeros.steps.UIReportSteps;
import com.aeros.steps.UserSteps;
import com.aeros.utils.RandomUtil;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import org.junit.After;
import org.junit.Before;
//import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
//import com.aeros.constants.Const;

import static com.aeros.constants.Const.DateFormat.*;
//import static com.aeros.constants.Const.MessageConstants.*;
import static com.aeros.constants.Const.Message.*;
import static com.aeros.constants.Const.Actions.*;
import static com.aeros.constants.Const.Dashboard.*;
//import static com.aeros.constants.Const.DateFormat.*;
//import static com.aeros.constants.Const.MessageConstants.*;
import static com.aeros.constants.Const.ModalWindow.*;
import static com.aeros.constants.Const.ModalWindowOperations.*;
import static com.aeros.constants.Const.ProjectNavBar.*;
import static com.aeros.constants.Const.Tooltip.*;

@RunWith(SerenityRunner.class)
@Story(AFLRequirements.ProjectsBaseFunctionality.class)
@WithTag(type = "feature", name = "REGRESSION")
public class ProjectsBaseFunctionalityStory {

   // private static final String PROJECT_ALREADY_EXISTS = null;

	@Managed(uniqueSession = true)
    private WebDriver driver;

    @Steps
    private UserSteps userSteps;

    @Steps
    private ProjectSteps projectSteps;

    @Steps
    private CommonSteps commonSteps;
    
    @Steps
    private UIReportSteps uiReportSteps;

    @Before
    public void setup() {
        System.setProperty("webdriver.driver", System.getProperty("webdriver.chrome.driver"));
    }

    private RestActions restActions = new RestActions();
    private Project project, projectCopy;
    private Project firstActiveProject = new Project("first_project");
    private Project secondActiveProject = new Project("second_project");
    private Project mergedProject = new Project("merged_project");
    private Project editedProject = new Project("edited_project", "edited comment");

    private Project projectWithSpecialCharacter = new Project("aeRos's project");
    private User adminUser = new User("admin").loadCredentials("valid.email", "valid.password");

    
    
    @Ignore
    @Test
    @Issue("#AEROS-MULTIPLE-PROJECTS WITH NO COMMENT")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create multiple projects with no comment")
    @WithTag(type = "feature", name = "adminUserProjectCreationStory1")
    public void can_create_10_100_projects_with_no_comment_as_admin() {
    	int num_projects = 10;
    	int i;
    	
    	userSteps.openWebBasePage();
		userSteps.logsInToAflWeb(adminUser);
		
    	for ( i = 0; i < num_projects; i++)
    	{
    		
    		project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
    		projectSteps.createAProject(project.getProjectName());
    		projectSteps.verifyCurrentCreatedProjectDate(project);
    		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
    		 		
    		
    	}
    }
   
    
        
   
    @Test
    @Issue("#AEROS-MULTIPLE-PROJECTS WITH A COMMENT")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create multiple projects with a comment")
    @WithTag(type = "feature", name = "adminUserProjectCreationStory")
    public void can_create_a_project_with_a_comment() {
        
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        int num_projects = 5;
    	int i;
        
    	for ( i = 0; i < num_projects; i++)
    	{
    		project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT), "test_comment");
    		projectSteps.createAProject(project.getProjectName(), project.getComment());
    		projectSteps.verifyCurrentCreatedProjectDate(project);
    		projectSteps.verifyProjectCommentPresence(project.getProjectName(), true);
    		projectSteps.verifyProjectCommentCorrect(project);
    	}
    }
    
    
    
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT WITH NO COMMENT")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project with no comment")
    @WithTag(type = "feature", name = "adminUserProjectCreationStory")
    public void can_create_a_project_with_no_comment_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        
        
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        
        projectSteps.createAProject(project.getProjectName());
        projectSteps.verifyCurrentCreatedProjectDate(project);
        projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
        
        /* Execute NUnit test*/
        /* Update report with UI report file */
        System.out.println("Reading UI report file\n");
        uiReportSteps.openUIReportFile(UI_REPORT_FILE_PATH);
        uiReportSteps.UI_Steps(); 
       
        
        
        
    }
    
	
    
    /*
    
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT WITH A COMMENT")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project with a comment")
    @WithTag(type = "feature", name = "adminUserProjectCreationStory")
    public void can_create_a_project_with_a_comment() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT), "test_comment");
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName(), project.getComment());
        projectSteps.verifyCurrentCreatedProjectDate(project);
        projectSteps.verifyProjectCommentPresence(project.getProjectName(), true);
        projectSteps.verifyProjectCommentCorrect(project);
    }

  
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT WITH SAME NAME")
    @Title("As a Admin user, I should not be able to create a additional project with the same name")
    @WithTag(type = "feature", name = "adminUserProjectCreationNegativeStory")
    public void can_not_create_a_project_with_the_same_name() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.waitForModalWindowVisibility();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.clicksCreateNewProjectButton();

        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        commonSteps.verifyErrorNotificationMessage(PROJECT_ALREADY_EXISTS, project.getProjectName());
    }

   
    @Ignore
    @Test
    @Issue("#AEROS-UPDATE PROJECT")
    @Title("As a Admin user, I should be able to edit the project ")
    @WithTag(type = "feature", name = "adminUserProjectEditingStory")
    public void can_edit_a_project_data_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.verifyCurrentCreatedProjectDate(project);
        projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
        projectSteps.verifyProjectsListSize(1);
        projectSteps.clickOnProjectOptionButton(project.getProjectName(), EDIT_ACTION);
        projectSteps.verifyModalWindowTitle(EDIT_PROJECT_WINDOW);
        projectSteps.enterProjectName(editedProject.getProjectName()).enterProjectComment(editedProject.getComment()).
                clickOnProjectSubmitButton();
        projectSteps.waitForProject(editedProject.getProjectName());
        projectSteps.verifyProjectNamePresence(editedProject.getProjectName());
        projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
        projectSteps.verifyProjectCommentPresence(editedProject.getProjectName(), true);
    }



    
    @Ignore
    @Test
    @Issue("#AEROS-1698")
    @Title("as org admin user I can reload hidden project")
    @WithTag(type = "feature", name = "adminUserProjectReloadingStory")
    public void can_reload_hidden_project_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.verifyCurrentCreatedProjectDate(project);
        projectSteps.makeProjectOperation(project.getProjectName(), HIDE_ACTION, CONFIRM_HIDE_BUTTON);
        projectSteps.verifyProjectIsRemoved(project.getProjectName());
        projectSteps.selectFilterOptionByText(HIDDEN_OPTION);
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectTabButtonPresence(project.getProjectName(), RELOAD_ACTION, true);
        projectSteps.verifyProjectTabButtonPresence(project.getProjectName(), HIDE_ACTION, false);
        projectSteps.makeProjectOperation(project.getProjectName(), RELOAD_ACTION, CONFIRM_SHOW_BUTTON);
        projectSteps.verifyProjectIsRemoved(project.getProjectName());
        projectSteps.selectFilterOptionByText(ACTIVE_OPTION);
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectTabButtonPresence(project.getProjectName(), HIDE_ACTION, true);
        projectSteps.verifyProjectNamePresence(project.getProjectName());
    }

    @Ignore
    @Test
    @Issue("#AEROS-1702")
    @Title("as org admin user I can merge two projects")
    @WithTag(type = "feature", name = "adminUserProjectMergingStory")
    public void can_merge_projects_as_admin() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.verifyMergeProjectButtonPresence(false);
        projectSteps.createAProject(firstActiveProject.getProjectName());
        projectSteps.verifyMergeProjectButtonPresence(false);
        projectSteps.verifyProjectNamePresence(firstActiveProject.getProjectName());
        projectSteps.createAProject(secondActiveProject.getProjectName());
        projectSteps.verifyProjectNamePresence(secondActiveProject.getProjectName());
        projectSteps.verifyMergeProjectButtonPresence(true);
        projectSteps.clickMergeProjectButton();
        projectSteps.mergeProjects(firstActiveProject.getProjectName(), secondActiveProject.getProjectName(), mergedProject.getProjectName());
        projectSteps.verifyProjectNamePresence(true, firstActiveProject.getProjectName(), secondActiveProject.getProjectName(),
                mergedProject.getProjectName());
        projectSteps.verifyCurrentCreatedProjectDate(mergedProject);
    }

   
    @Ignore
    @Test
    @Issue("#AEROS-1710")
    @Title("as org admin user I can check project information")
    @WithTag(type = "feature", name = "adminUserProjectInformationStory")
    public void can_check_page_instructions_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT), "test_comment");
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName(), project.getComment());
        Project activeProject = projectSteps.loadAdditionalInfo(project.getProjectName());
        projectSteps.clickOnProjectOptionButton(project.getProjectName(), DETAILS_ACTION);
        Project projectInfoBlockDetails = projectSteps.getProjectInfoBlockDetails();
        projectSteps.verifyProjectDetailsIdentity(activeProject, projectInfoBlockDetails);
    }

   @Ignore
    @Test
    @Issue("#AEROS-1718")
    @Title("as org admin user I can search existed project")
    @WithTag(type = "feature", name = "adminUserProjectSearchStory")
    public void can_search_existed_project_as_admin()  {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.waitForModalWindowVisibility();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.createAProject(projectWithSpecialCharacter.getProjectName());
        projectSteps.enterValueIntoSearchField(projectWithSpecialCharacter.getProjectName());
        projectSteps.verifyProjectsListSize(1);
        projectSteps.verifyProjectNamePresence(projectWithSpecialCharacter.getProjectName());
        projectSteps.verifyProjectIsRemoved(project.getProjectName());
    }

    @Ignore
    @Test
    @Issue("#AEROS-1719")
    @Title("as org admin user I can search project with no result")
    @WithTag(type = "feature", name = "adminUserProjectNoResultSearchStory")
    public void can_search_project_with_no_result_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(firstActiveProject.getProjectName());
        projectSteps.createAProject(secondActiveProject.getProjectName());
        projectSteps.enterValueIntoSearchField(project.getProjectName());
        projectSteps.waitForProjectAbsence(firstActiveProject.getProjectName());
        projectSteps.waitForProjectAbsence(secondActiveProject.getProjectName());
        projectSteps.verifyProjectNamePresence(false, firstActiveProject.getProjectName(),
                secondActiveProject.getProjectName(), project.getProjectName());
    }

    @Ignore
    @Test
    @Title("as org admin user I can filter project")
    @WithTag(type = "feature", name = "adminUserProjectFilteringStory")
    public void can_filter_projects_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        userSteps.openWebBasePage();
       userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(firstActiveProject.getProjectName());
        projectSteps.createAProject(secondActiveProject.getProjectName());
        projectSteps.createAProject(project.getProjectName());
        projectSteps.makeProjectOperation(project.getProjectName(), HIDE_ACTION, CONFIRM_HIDE_BUTTON);
        projectSteps.verifyProjectStatus("active");
        projectSteps.verifyProjectIsRemoved(project.getProjectName());
        projectSteps.verifyProjectNamePresence(true, firstActiveProject.getProjectName(), secondActiveProject.getProjectName());
        projectSteps.selectFilterOptionByText("hidden");
        projectSteps.waitForProjectNameTextToAppear(project.getProjectName());
        projectSteps.waitForProjectNameTextToDisappear(firstActiveProject.getProjectName());
        projectSteps.waitForProjectNameTextToDisappear(secondActiveProject.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.verifyProjectNamePresence(false, firstActiveProject.getProjectName(), secondActiveProject.getProjectName());
        projectSteps.selectFilterOptionByText("activeAndHidden");
        projectSteps.waitForProjectNameTextToAppear(project.getProjectName());
        projectSteps.waitForProjectNameTextToAppear(firstActiveProject.getProjectName());
        projectSteps.waitForProjectNameTextToAppear(secondActiveProject.getProjectName());
        projectSteps.verifyProjectNamePresence(true, firstActiveProject.getProjectName(), secondActiveProject.getProjectName(),
                project.getProjectName());
    }

    @Ignore
    @Test
    @Issue("#AEROS-1731")
    @Title("as org admin user I can create a copy of project with comment modifications")
    @WithTag(type = "feature", name = "adminUserProjectCopyingModificationsStory")
    public void can_create_project_copy_with_comment_modifications_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT), "test_comment");
        projectCopy = new Project("Copy of ".concat(project.getProjectName()));
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName(), project.getComment());
        projectSteps.clickOnProjectOptionButton(project.getProjectName(), COPY_ACTION);
        projectSteps.verifyModalWindowProjectNameValue(projectCopy.getProjectName());
        projectSteps.clearModalWindowProjectCommentValue();
        projectSteps.clickOnCreateProjectSubmitButton();
        projectSteps.waitForProjectNameTextToAppear(projectCopy.getProjectName());
        projectSteps.verifyProjectNamePresence(projectCopy.getProjectName());
        projectSteps.verifyProjectCommentPresence(projectCopy.getProjectName(), false);
    }

    @Ignore
    @Test
    @Issue("#AEROS-1730")
    @Title("as org admin user I can create a copy of project")
    @WithTag(type = "feature", name = "adminUserProjectCopyingStory")
    public void can_create_a_copy_of_project_as_admin() {
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
        String projectNameCopy = "Copy of ".concat(project.getProjectName());
        projectCopy = new Project(projectNameCopy);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.waitForModalWindowVisibility();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.clickOnProjectOptionButton(project.getProjectName(), COPY_ACTION);
        projectSteps.verifyModalWindowProjectNameValue(projectCopy.getProjectName());
        projectSteps.clickOnCreateProjectSubmitButton();
        projectSteps.waitForProjectNameTextToAppear(projectCopy.getProjectName());
        projectSteps.verifyProjectNamePresence(projectCopy.getProjectName());
    }
    
    */
    
/*
    @After
    public void tearDown() {
        driver.quit();
       restActions.deleteProjects();
    }
    */
}
