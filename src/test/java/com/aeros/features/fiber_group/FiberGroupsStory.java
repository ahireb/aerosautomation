package com.aeros.features.fiber_group;

import com.aeros.model.FiberGroup;
import com.aeros.model.XamarinCmd;

import com.aeros.model.Project;
import com.aeros.model.User;
import com.aeros.services.restful.api.RestActions;
//import com.aeros.requirements.AFLRequirements;
//import com.aeros.requirements.AFLRequirements;
import com.aeros.requirements.AFLRequirements.FiberGroups;
import com.aeros.steps.CommonSteps;
import com.aeros.steps.FiberGroupSteps;
import com.aeros.steps.ProjectSteps;
import com.aeros.steps.ToolSteps;
import com.aeros.steps.UIReportSteps;
import com.aeros.steps.UserSteps;
import com.aeros.utils.RandomUtil;

//import ch.lambdaj.util.NotUniqueItemException;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;
import net.thucydides.core.annotations.findby.By;

//import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;

//import java.util.List;
import java.util.Set;

import static com.aeros.constants.Const.Actions.*;
import static com.aeros.constants.Const.Dashboard.*;
import static com.aeros.constants.Const.FilePath.*;
import static com.aeros.constants.Const.DateFormat.*;
import static com.aeros.constants.Const.MessageConstants.*;
//import static com.aeros.constants.Const.ModalWindow.*;
//import static com.aeros.constants.Const.ModalWindowOperations.*;
//import static com.aeros.constants.Const.ProjectNavBar.*;
//import static com.aeros.constants.Const.Tooltip.*;

@RunWith(SerenityRunner.class)
@Story(FiberGroups.class)
@WithTag(type = "feature", name = "REGRESSION")
public class FiberGroupsStory {

    @Managed(uniqueSession = true)
    private WebDriver driver;

    @Steps
    private UserSteps userSteps;
    
    @Steps
    private UIReportSteps uiReportSteps;

    @Steps
    private FiberGroupSteps fiberGroupSteps;

    @Steps
    private ProjectSteps projectSteps;
    
    @Steps
    private ToolSteps toolSteps;

    @Steps
    private CommonSteps commonSteps;
    
    JavascriptExecutor jse = (JavascriptExecutor) driver;
    
 
    
    private RestActions restActions = new RestActions();
    private User adminUser = new User("admin").loadCredentials("valid.email", "valid.password");
    //private Project project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));
    private Project project = new Project("aeRos-LM-integration-project");
    private Project project_name = new Project("Project SM 1600");
    private FiberGroup defaultFiberGroup = FiberGroup.builder("FG-1").build();
    private FiberGroup fiberGroupName = FiberGroup.builder("FiberGroup 1").build();
    private FiberGroup editedFiberGroup = FiberGroup.builder("edited_fiber_group").build();
    private Set<String> selectedFiberGroupItems;
    private FiberGroup fiberGroupWithDigits = FiberGroup.builder("123").build();
    private FiberGroup fiberGroupWithSymbols = FiberGroup.builder("Aeros's_fiber_group").build();
    private FiberGroup fiberGroupWithSpaces = FiberGroup.builder("  Fiber group  ").build();
    private FiberGroup fullDetailsFiberGroup = FiberGroup.builder(RandomUtil.generateRandomFormat("fiber_group", FULL_FORMAT)).
            setFiberType("OM3").setStartingFiberNumber("5").setNumberOfFibers("10").setNumberOfConnections("3").setNumberOfSplices("1").
            setEnd1Name("EndPoint1").setEnd1Connector("LC").setEnd1ConnectorOption("Bulkhead").
            setEnd2Name("EndPoint2").setEnd2Connector("FC/APC").setEnd2ConnectorOption("Patchcord").build();

    private String messageConstraint;
    private FiberGroup randomFiberGroup = FiberGroup.builder(RandomUtil.generateRandomFormat("fiber_group", FULL_FORMAT)).build();

    

    @Before
    public void setup() {
        System.setProperty("webdriver.driver", System.getProperty("webdriver.chrome.driver"));
    }

  /*  
   * @Ignore
    @Test
    @Issue("#AEROS-1754")
    @Title("admin user can verify fiber groups page")
    @WithTag(type = "feature", name = "adminUserFiberGroupsSmokeStory")
    public void can_verify_fiber_groups_page() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.verifyHeaderPageLabel(project.getProjectName());
        fiberGroupSteps.verifyProjectNavBarOptionIsSelected(FIBERGROUPS);
        fiberGroupSteps.verifyPanelHeadingsPresence("Media Types", "Test Setups");
    }
    
    */
    //This test case is to check '?' working or not.
    @Ignore
    @Test
    @Issue("#AEROS-PROJECTDASHBOARD")
    @Title("As a Admin user, I should be able to login to aeRos and be able to see Instructions page for Project Dashboard Page")
    @WithTag(type = "feature", name = "Help Page")
    public void helpOptionWorkingOrNot() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.checkProjectListPresent();
         toolSteps.selectHelpButton();
         projectSteps.checkHelpPagePresent();
         projectSteps.clickToCloseHelpPage();
         Thread.sleep(1000);
            
    }
    
    // Smoke test aeRos-LM integration test for Single mode
  
    @Test
    @Issue("#LM-aeRos integration test")
    @Title("SmokeTestAerosGuided")
    @WithTag(type = "feature", name = "Help Page")
    public void SmokeTestAerosGuided() throws InterruptedException {
    	//XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project_name.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project_name.getProjectName());
        projectSteps.verifyProjectNamePresence(project_name.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project_name.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(fiberGroupName.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(fiberGroupName.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(fiberGroupName);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, fiberGroupName.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
        
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectMultiMode();
        toolSteps.addOTDRToolName("SmokeTestSM-tool");
        toolSteps.selectRCFromDropDown("user");
        toolSteps.waitForRCCableLength();
        toolSteps.enterRCLength("150");
               
    }
    
    
    
    
  //This test case is to check Active/Hidden/Active-Hidden options are working or not.
    @Ignore
    @Test
    @Issue("#AEROS-PROJECTDASHBOARD")
    @Title("As a Admin user, I should be able to login to aeRos and be able to see active, hidden and Active-Hidden projects")
    @WithTag(type = "feature", name = "Show project list(Active,Hidden,Active-Hidden")
    public void active_HiddenOptionWorkingOrNot() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.checkProjectListPresent();
        projectSteps.selectActiveFromDropDownList("Active");
        projectSteps.checkProjectListPresent();
        Thread.sleep(3000);
        projectSteps.selectActiveFromDropDownList("Hidden");
        projectSteps.checkProjectListPresent();
        Thread.sleep(3000);
        projectSteps.selectActiveFromDropDownList("Active and Hidden");
        projectSteps.checkProjectListPresent();
        Thread.sleep(3000);
      }
    
    
    /*This test will create project, FG and OTDR tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void can_create_otdr_project() throws InterruptedException {
    	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        
        
        
        
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("Automation_OTDR_Tool");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("Automation_OTDR_Tool");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Automation_OTDR_Tool");
        toolSteps.selectToolByName("Automation_OTDR_Tool");               
        Thread.sleep(5000);
        xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        uiReportSteps.openUIReportFile(UI_REPORT_FILE_PATH);
        uiReportSteps.UI_Steps(); 
        Thread.sleep(5000); 
        }
       
        
        
        
        //xcmd.execXamarinTest("SyncWithAerosByProjectName");
      //Project-1  
  
    //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start and End connector of 'Link Pass/Fail criteria' Included 
   @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-include and Include-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIIII() throws InterruptedException {
    	
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
      //  toolSteps.selectRCFromDropDown("User");
     //   toolSteps.waitForRCCableLength();
      // toolSteps.enterRCLength("150");
        //toolSteps.selectEventStartConnector("Include");
        
        toolSteps.selectEventEndConnector("Include");
       // Thread.sleep(1000);
       // ((JavascriptExecutor)driver).executeScript(“ToolsPage.PopupWindow.scrollTo(document.body.scrollHeight,0)”);
       
       
     //   ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toolSteps.scrollUpWindow());
      //  Thread.sleep(500); 
        Thread.sleep(5000);
        toolSteps.clickOnLinkPassFailDownArrow();
        jse.executeScript("scroll(250,0)");
      
        
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(2000);
                toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  /*  @Test 
    public void lm_app_test1()
    {
    	NUnit3 app_test_1;
    	Read file and prepare report
    }
    
    @Test
    public void match_tool()
    {
    	NUnit3 mathc_tool;
    	Read file 
    }
    
    @Test 
    public void lm_app_test2()
    {
    	NUnit3 app_test_2;
    	Read file and prepare report
    }*/
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Included and End Connector - Excluded for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-include and Include-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIIIE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Included for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-include and Exclude-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIIEI() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Exclude for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-include and Exclude-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIIEE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    //Project-2
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start and End connector of 'Link Pass/Fail criteria' Included 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-Exclude and Include-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIEII() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Included and End Connector - Excluded for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-Exclude and Include-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIEIE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Included for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-Exclude and Exclude-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIEEI() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Exclude for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with include-Exclude and Exclude-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorIEEE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Include");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    //Project-3
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start and End connector of 'Link Pass/Fail criteria' Included 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-include and Include-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEIII() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Included and End Connector - Excluded for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-include and Include-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEIIE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Included for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-include and Exclude-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEIEI() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Exclude for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-Include and Exclude-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEIEE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Include");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    //Project-4
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start and End connector of 'Link Pass/Fail criteria' Included 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-Exclude and Include-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEEII() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Included and End Connector - Excluded for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-Exclude and Include-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEEIE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Include in Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Included for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclude-Exclude and Exclude-Include settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEEEI() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Include in Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
  //Create Project/FGs/OTDR tool with Start and End connector of 'Event Pass/Fail criteria' Included and Start Connector - Exclude and End Connector - Exclude for "Link Pass/Fail criteria" 
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR tool with Exclue-Exclude and Exclude-Exclue-Exclude settings")
    @WithTag(type = "feature", name = "Add OTDR tool to project")
    public void canCreateOtdrToolWithAllConnectorEEEE() throws InterruptedException {
    //	XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName("OTDR_Tool_Include_Include");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForLCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectLCFromDropDown("User");
        toolSteps.waitForRCCableLength();
        toolSteps.enterLCLength("150");
        toolSteps.selectEventStartConnector("Exclude");
        toolSteps.selectEventEndConnector("Exclude");
        toolSteps.selectLinkStartConnector("Exclude from Link Calculations");
        toolSteps.selectLinkEndConnector("Exclude from Link Calculations");
        Thread.sleep(1000);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("OTDR_Tool_Include_Include");
        toolSteps.selectToolByName("OTDR_Tool_Include_Include");               
        Thread.sleep(5000);
   /*     xcmd.execXamarinTest("aeorsintegrationtest");
        Thread.sleep(1000);
        toolSteps.waitForResultButton();   
        toolSteps.clickOnResultButton();
        toolSteps.waitForToolVisibility(defaultFiberGroup.getFiberGroupName().trim());
        //driver.manage().window().maximize();
        toolSteps.clickOnTraceButton();
        Thread.sleep(5000);*/
       
    }
    
    //Page header includes aeRos logo
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION_COMBINED")
    @Title("As a Admin user, I should be able to login to aeRos and be able to see aeRos logo on page header")
    @WithTag(type = "feature", name = "aeRos logo")
    public void can_see_aeros_logo_on_page_header() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        toolSteps.aeRosLogoPresent();
        Thread.sleep(1000);
                
    }
    
    //Organizational_menu is present or not
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION_COMBINED")
    @Title("As a Admin user, I should be able to login to aeRos and be able to see organizational Menu")
    @WithTag(type = "feature", name = "aeRos Organizational Menu")
    public void can_see_organizational_menu() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        toolSteps.organizationalMenuPresent();
        Thread.sleep(1000);
                
    }
    
    
    
    
    
    
    //Combined inspection tool test
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION_COMBINED")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool with options")
    @WithTag(type = "feature", name = "Add Inspection tool to project with options")
    public void can_create_a_fiber_group_With_inspection_tool_options() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        
        
        /* Create FG-1*/
        String fg_name = "FG-1";
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(fg_name);
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, fg_name);
        //Thread.sleep(1000);
        /*Attach Inspection tool with both ends */   
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("Ins_Tool_Both_Ends_None", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);
        toolSteps.selectBothEndsbutton();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        // Scroll till we see the added tool
        
        toolSteps.waitForToolVisibility(tool_name);
        
         //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(fg_name.trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(5000);
        
        
        /* Create FG-2*/
        fg_name = "FG-2";
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(fg_name);
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, fg_name);
        //Thread.sleep(1000);
        /*Attach Inspection tool with end1 */   
        tool_name = RandomUtil.generateRandomFormat("Ins_Tool_End1_None", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(fg_name.trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(5000);
        
        /* Create FG-3*/
        fg_name = "FG-3";
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(fg_name);
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, fg_name);
        //Thread.sleep(1000);
        /*Attach Inspection tool with end2 */
        
        tool_name = RandomUtil.generateRandomFormat("Ins_Tool_End2_None", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name); 
        //toolSteps.selectOnlyEnd1button();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(fg_name.trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(5000);
        
        
        /* Create FG-3*/
        fg_name = "FG-4";
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(fg_name);
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, fg_name);
        //Thread.sleep(1000);
        /*Attach Inspection tool with both ends and IEC45 */
        
        tool_name = RandomUtil.generateRandomFormat("Ins_Tool_Both_Ends_IEC45", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);       
        //toolSteps.selectOnlyEnd2Button();
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(fg_name.trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(5000);
        
    }    
    
    
    
    
    
    //Below all test cases are to create Single mode Inspection tool with either both ends/Only End1/Only End2 and 6 different P/F Standards.
    
    /*This test will create project, FG and Inspection tool with Both Ends and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_none() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none");
        toolSteps.selectBothEndsbutton();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none");
        toolSteps.selectToolByName("Auto_none");
        Thread.sleep(5000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_none_and_end1() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none1");
     //   toolSteps.selectBothEndsbutton();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none1");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none1");
        toolSteps.selectToolByName("Auto_none1");
        Thread.sleep(5000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_none_and_end2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none2");
     //   toolSteps.selectBothEndsbutton();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none2");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none2");
        toolSteps.selectToolByName("Auto_none2");
        Thread.sleep(3000);      
    }    
    
   
    
    /*This test will create project, FG and Inspection tool with Both Ends and IEC 61300-3-35 PC-SM-RL-45 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Automation_Inspection_Tool_new1");
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Automation_Inspection_Tool_new1");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Automation_Inspection_Tool_new1");
        toolSteps.selectToolByName("Automation_Inspection_Tool_new1");
        Thread.sleep(10000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IEC 61300-3-35 PC-SM-RL-45 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_and_Inspection_tool() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Insp_End1");
       // toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Insp_End1");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Insp_End1");
        toolSteps.selectToolByName("Insp_End1");
        Thread.sleep(1000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IEC 61300-3-35 PC-SM-RL-45 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_and_Inspection_tool_with_End2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Insp_End2");
       // toolSteps.selectBothEndsbutton();
      //  toolSteps.selectOnlyEnd1button();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Insp_End2");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Insp_End2");
        toolSteps.selectToolByName("Insp_End2");
        Thread.sleep(1000);      
    }   
    
    /*This test will create project, FG and Inspection tool with Both Ends and IEC 61300-3-35 APC SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_APC_Both() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IEC 61300-3-35 APC SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_APC_End1() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IEC 61300-3-35 APC SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_APC_End2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    /*This test will create project, FG and Inspection tool with Both Ends and IEC 61300-3-35 PC-SM-RL-26 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_26_Both() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IEC 61300-3-35 PC-SM-RL-26 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_26_End1() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IEC 61300-3-35 PC-SM-RL-26 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IEC_26_End2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
   
    /*This test will create project, FG and Inspection tool with Both Ends and AT&T SM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_ATandT_Both() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
        toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and AT&T SM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_ATandT_End1() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and AT&T SM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_ATandT_End2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    
    
    /*This test will create project, FG and Inspection tool with Both Ends and IPC 610-DC 16.1.1/2 SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IPC_Both() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIPCRule();
      //  toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IPC 610-DC 16.1.1/2 SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IPC_End1() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
        toolSteps.selectIPCRule();
     //   toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IPC 610-DC 16.1.1/2 SM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void can_create_a_fiber_group_With_IPC_End2() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
        toolSteps.selectIPCRule();
        //toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    // Below all test cases are to create MultiMode Inspection tool with either Both Ends/Only End1/Only End2 and 4 different P/F Standards
    
    /*This test will create project, FG and Inspection tool with Both Ends and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGWithBothandNone() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
      //  toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
      //  toolSteps.selectIPCRule();
      //  toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGwithEnd1andNone() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
     //   toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
       // toolSteps.selectIPCRule();
     //   toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and None P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateFGwithEnd2andNone() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
       // toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
       // toolSteps.selectIPCRule();
        //toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
        toolSteps.selectNonePassFailStandard();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    
    
    /*This test will create project, FG and Inspection tool with Both Ends and IEC_61300-3-35 PC-MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGWithBothandIECPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
      //  toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
      //  toolSteps.selectIPCRule();
      //  toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
     //   toolSteps.selectNonePassFailStandard();
        toolSteps.selectIECPCMMRule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IEC_61300-3-35 PC-MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGwithEnd1andIECPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
     //   toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
       // toolSteps.selectIPCRule();
     //   toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
        toolSteps.selectIECPCMMRule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IEC_61300-3-35 PC-MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateFGwithEnd2andIECPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
       // toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
       // toolSteps.selectIPCRule();
        //toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
      //  toolSteps.selectNonePassFailStandard();
        toolSteps.selectIECPCMMRule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    /*This test will create project, FG and Inspection tool with Both Ends and AT&T MM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGWithBothandATT() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
      //  toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
      //  toolSteps.selectIPCRule();
      //  toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
     //   toolSteps.selectNonePassFailStandard();
      //  toolSteps.selectIECPCMMRule();
        toolSteps.selectAT_TMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and AT&T MM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGwithEnd1andATT() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
     //   toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
       // toolSteps.selectIPCRule();
     //   toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
     //   toolSteps.selectIECPCMMRule();
        toolSteps.selectAT_TMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and AT&T MM TP-76461 P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateFGwithEnd2andATT() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
       // toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
       // toolSteps.selectIPCRule();
        //toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
      //  toolSteps.selectNonePassFailStandard();
        //toolSteps.selectIECPCMMRule();
        toolSteps.selectAT_TMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
    
    /*This test will create project, FG and Inspection tool with Both Ends and IPC 610-DC 16.1.1/2 MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGWithBothandIPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
      //  toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none4");
        toolSteps.selectBothEndsbutton();
      //  toolSteps.selectIPCRule();
      //  toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
       // toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
     //   toolSteps.selectNonePassFailStandard();
      //  toolSteps.selectIECPCMMRule();
        //toolSteps.selectAT_TMM();
        toolSteps.selectIPCMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none4");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none4");
        toolSteps.selectToolByName("Auto_none4");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End1 and IPC 610-DC 16.1.1/2 MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateMMFGwithEnd1andIPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
     //   toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_none5");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd1button();
       // toolSteps.selectIPCRule();
     //   toolSteps.selectATandTRule();
     //   toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
       // toolSteps.selectNonePassFailStandard();
     //   toolSteps.selectIECPCMMRule();
        //toolSteps.selectAT_TMM();
        toolSteps.selectIPCMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_none5");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_none5");
        toolSteps.selectToolByName("Auto_none5");
        Thread.sleep(3000);      
    }    
    
    /*This test will create project, FG and Inspection tool with Only End2 and IPC 610-DC 16.1.1/2 MM P/F Standard selected*/
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and Inspection tool")
    @WithTag(type = "feature", name = "Add Inspection tool to project")
    public void canCreateFGwithEnd2andIPCMM() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
       // toolSteps.selectSingleMode();
        toolSteps.selectMultiMode();
        toolSteps.addInspectionToolName("Auto_APC");
      //  toolSteps.selectBothEndsbutton();
        toolSteps.selectOnlyEnd2Button();
       // toolSteps.selectIPCRule();
        //toolSteps.selectATandTRule();
      //  toolSteps.selectIEC26Rule();
     //   toolSteps.selectIECAPCRule();
     //   toolSteps.selectPassFailRule();
      //  toolSteps.selectNonePassFailStandard();
        //toolSteps.selectIECPCMMRule();
      //  toolSteps.selectAT_TMM();
        toolSteps.selectIPCMM();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility("Auto_APC");
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility("Auto_APC");
        toolSteps.selectToolByName("Auto_APC");
        Thread.sleep(2000);      
    }    
    
     
    
    
    
    /*This test will create project, FG and OLTS tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OLTS")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OLTS tool")
    @WithTag(type = "feature", name = "Add OLTS tool to project")
    public void can_create_a_OLTS_project() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
		projectSteps.createAProject(project.getProjectName());
		projectSteps.verifyCurrentCreatedProjectDate(project);
		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
		 		
      //  projectSteps.clicksCreateNewProjectButton();
      //  projectSteps.isModalContentDisplayed();
       // projectSteps.enterProjectName(project.getProjectName());
       	// projectSteps.clickOnProjectSubmitButton();
       // projectSteps.waitForProject(project.getProjectName());
       // projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
      //  Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("Olts_tool", FULL_FORMAT);
      
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OLTS");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);
        toolSteps.enterDescription("Tool for OLTS");
        toolSteps.selectBidirectionalOptionForOLTS();
        toolSteps.selectOneCordReferenceOptionForOLTS();
            
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        Thread.sleep(1000);     
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(3000);    
    }    
    
    
    /*This test will create project, FG and OTDR and Inspection tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR and Inspection tools")
    @WithTag(type = "feature", name = "Add OTDR and Inspection tool to project")
    public void can_create_otdrandInspection_project() throws InterruptedException {
    	//XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
		projectSteps.createAProject(project.getProjectName());
		projectSteps.verifyCurrentCreatedProjectDate(project);
		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
     //   projectSteps.clicksCreateNewProjectButton();
       // projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
      //  projectSteps.enterProjectName(project.getProjectName());
      //  projectSteps.clickOnProjectSubmitButton();
      //  projectSteps.waitForProject(project.getProjectName());
      //  projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
     //   Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("OTDR_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName(tool_name);
        Thread.sleep(1500);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        
        toolSteps.waitForToolVisibility(tool_name);
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(500);
        String tool_name1;
        tool_name1 = RandomUtil.generateRandomFormat("Insp_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name1);
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name1);
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name1);
        toolSteps.selectToolByName(tool_name1);
        Thread.sleep(500);      
        
    }
    
    /*This test will create project, FG and OTDR and OLTS tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OTDR/OLTS")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR and OLTS tools")
    @WithTag(type = "feature", name = "Add OTDR and OLTS tool to project")
    public void can_create_otdrandOLTS_project() throws InterruptedException {
    	//XamarinCmd  xcmd = new XamarinCmd(NUNIT_TOOL_PATH, XAMARIN_DLL_PATH);
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
		projectSteps.createAProject(project.getProjectName());
		projectSteps.verifyCurrentCreatedProjectDate(project);
		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
     //   projectSteps.clicksCreateNewProjectButton();
     //   projectSteps.isModalContentDisplayed();
        //projectSteps.enterProjectName(project.getProjectName());
      //  projectSteps.enterProjectName(project.getProjectName());
       // projectSteps.clickOnProjectSubmitButton();
       // projectSteps.waitForProject(project.getProjectName());
     //   projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.setFiberCount(1);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
       
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
      //  Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("OTDR_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName(tool_name);
        
        Thread.sleep(1500);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(500);
        String tool_name1;
        tool_name1 = RandomUtil.generateRandomFormat("OLTS_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OLTS");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name1);
        toolSteps.enterDescription("Tool for OLTS");
        toolSteps.selectBidirectionalOptionForOLTS();
        toolSteps.selectOneCordReferenceOptionForOLTS();
            
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name1);
        Thread.sleep(1000);     
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name1);
        toolSteps.selectToolByName(tool_name1);
        Thread.sleep(500);    
    }
    
    /*This test will create project, FG and OLTS and Inspection tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/OLTS/INSPECTION")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OLTS and Inspection tools")
    @WithTag(type = "feature", name = "Add OLTS and Inspection tool to project")
    public void can_create_a_OLTSandInspection_project() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
		projectSteps.createAProject(project.getProjectName());
		projectSteps.verifyCurrentCreatedProjectDate(project);
		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
       // projectSteps.clicksCreateNewProjectButton();
       // projectSteps.isModalContentDisplayed();
     //   projectSteps.enterProjectName(project.getProjectName());
      //  projectSteps.clickOnProjectSubmitButton();
      //  projectSteps.waitForProject(project.getProjectName());
     //   projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
    //    Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("OLTS_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OLTS");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);
        toolSteps.enterDescription("Tool for OLTS");
        toolSteps.selectBidirectionalOptionForOLTS();
        toolSteps.selectOneCordReferenceOptionForOLTS();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        //Thread.sleep(500);     
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(500);  
        String tool_name1;
        tool_name1 = RandomUtil.generateRandomFormat("Insp_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name1);
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name1);
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name1);
        toolSteps.selectToolByName(tool_name1);
        Thread.sleep(500);      
     }    
    
    /*This test will create project, FG and OTDR,Inspection and OLTS tool */
    @Ignore
    @Test
    @Issue("#AEROS-PROJECT/FG/INSPECTION/OLTS/OTDR")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project,fiber group and OTDR,OLTS and Inspection tools")
    @WithTag(type = "feature", name = "Add OLTS,Inspection and OTDR tool to project")
    public void can_create_a_OLTSandInspectionandOTDR_project() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        project = new Project(RandomUtil.generateRandomFormat("project", FULL_FORMAT));    	
		projectSteps.createAProject(project.getProjectName());
		projectSteps.verifyCurrentCreatedProjectDate(project);
		projectSteps.verifyProjectCommentPresence(project.getProjectName(), false);
    //    projectSteps.clicksCreateNewProjectButton();
    //    projectSteps.isModalContentDisplayed();
    //    projectSteps.enterProjectName(project.getProjectName());
    //    projectSteps.clickOnProjectSubmitButton();
    //    projectSteps.waitForProject(project.getProjectName());
    //    projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        System.out.println("Waiting for project header to disappear\n");
        //Thread.sleep(500);
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        System.out.println("Waiting for project header to disappear DONE\n");
        Thread.sleep(1000);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);
        fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
    //    Thread.sleep(1000);
   //     fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        String tool_name;
        tool_name = RandomUtil.generateRandomFormat("OLTS_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OLTS");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name);
        toolSteps.enterDescription("Tool for OLTS");
        toolSteps.selectBidirectionalOptionForOLTS();
        toolSteps.selectOneCordReferenceOptionForOLTS();
            
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name);
        Thread.sleep(1000);     
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name);
        toolSteps.selectToolByName(tool_name);
        Thread.sleep(500);  
        String tool_name1;
        tool_name1 = RandomUtil.generateRandomFormat("Insp_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("Inspection");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addInspectionToolName(tool_name1);
        toolSteps.selectBothEndsbutton();
        toolSteps.selectIEC45Rule();
        toolSteps.clickOnSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name1);
        
        
        //toolSteps.clickOnInformationButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name1);
        toolSteps.selectToolByName(tool_name1);
        Thread.sleep(500);  
        String tool_name2;
        tool_name2 = RandomUtil.generateRandomFormat("OTDR_tool", FULL_FORMAT);
        toolSteps.selectToolAddAction();
        toolSteps.selectToolFromDropDown("OTDR");
        toolSteps.selectTool();
        toolSteps.selectSingleMode();
        toolSteps.addOTDRToolName(tool_name2);
        
        Thread.sleep(1500);
        toolSteps.clickOnOTDRToolSaveButtonToSave();
        toolSteps.waitForToolVisibility(tool_name2);
        
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName().trim());
        toolSteps.waitForToolVisibility(tool_name2);
        toolSteps.selectToolByName(tool_name2);
        Thread.sleep(1000);
     
     }    
   
    // Create 100 FG 
    @Ignore
    @Test
    @Issue("#AEROS-1983")
    @Title("As a Admin user, I should be able to login to aeRos and be able to create a project with 100 FGs")
    public void create_multiple_fgs() throws InterruptedException 
    {
    	  String chFGName = "FG-";	
    	  int num_fgs = 75;
    	  userSteps.openWebBasePage();
          userSteps.logsInToAflWeb(adminUser);
          projectSteps.clicksCreateNewProjectButton();
          projectSteps.isModalContentDisplayed();
          projectSteps.enterProjectName(project.getProjectName());
          projectSteps.clickOnProjectSubmitButton();
          projectSteps.waitForProject(project.getProjectName());
          projectSteps.verifyProjectNamePresence(project.getProjectName());
          projectSteps.goToProjectFiberGroupPage(project.getProjectName());
          System.out.println("Waiting for project header to disappear\n");
          //Thread.sleep(500);
          fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
          System.out.println("Waiting for project header to disappear DONE\n");
        //  Thread.sleep(1000);
          
          for(int i=0 ; i< num_fgs; i++)
          {
        	  String name = chFGName + (i +1); 
        	  System.out.println("FG Name prepared  := " + name + "\n");
        	  Thread.sleep(500);
	          fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
	          fiberGroupSteps.isModalContentBlockDisplayed();
	          fiberGroupSteps.isCreateFiberGroupButtonActive(false);
	          fiberGroupSteps.enterFiberGroupName(name);
	          fiberGroupSteps.isCreateFiberGroupButtonActive(true);
	          fiberGroupSteps.selectFiberTypeOption(defaultFiberGroup.getFiberType());
	          fiberGroupSteps.setFiberCount(1);
	          fiberGroupSteps.clickOnCreateFiberGroupButton();
	          fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
	          commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, name);
          //commonSteps.verifySuccessfulNotificationMessage("Fiber group 'undefined' added", defaultFiberGroup.getFiberGroupName());
         // Thread.sleep(1000);
          }
          
  }    

    

    /*
    @Test
    @Issue("#AEROS-1767")
    @Title("admin user can not create a fiber group with the same name")
    @WithTag(type = "feature", name = "adminUserFiberGroupCreationNegativeStory")
    public void can_not_create_a_fiber_group_with_the_same_name() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupSizeCorrect(1);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ALREADY_EXISTS, defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupSizeCorrect(1);
    }

    @Test
    @Issue("#AEROS-1771")
    @Title("admin user can edit a fiber group")
    @WithTag(type = "feature", name = "adminUserFiberGroupEditingStory")
    public void can_edit_a_fiber_group() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.selectFiberGroupAction(defaultFiberGroup.getFiberGroupName(), EDIT_ACTION);
        fiberGroupSteps.isModalWindowTitleCorrect(EDIT_FIBER_GROUP_WINDOW);
        fiberGroupSteps.editFiberGroupName(editedFiberGroup.getFiberGroupName());
        fiberGroupSteps.clickOnSubmitButton();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_UPDATED, editedFiberGroup.getFiberGroupName());
        fiberGroupSteps.isFiberGroupPresent(editedFiberGroup);
        fiberGroupSteps.isFiberGroupAbsent(defaultFiberGroup);
    }

    @Test
    @Issue("#AEROS-1772")
    @Title("admin user can delete a fiber group")
    @WithTag(type = "feature", name = "adminUserFiberGroupDeletingStory")
    public void can_delete_a_fiber_group() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, defaultFiberGroup.getFiberGroupName());
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);

        fiberGroupSteps.verifyFiberGroupIsSelected(defaultFiberGroup.getFiberGroupName(), false);
        fiberGroupSteps.verifyFiberGroupActionIsEnabled(DELETE_ACTION, false);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.verifyFiberGroupIsSelected(defaultFiberGroup.getFiberGroupName(), true);
        fiberGroupSteps.verifyFiberGroupActionIsEnabled(DELETE_ACTION, true);
        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.isModalContentBlockDisplayed();
        fiberGroupSteps.isModalWindowTitleCorrect(DELETE_FIBER_GROUPS_WINDOW);
        fiberGroupSteps.verifyDeletingFiberGroupNamesCorrect(selectedFiberGroupItems);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUPS_REMOVED);
        fiberGroupSteps.isFiberGroupAbsent(defaultFiberGroup);
    }

    @Test
    @Issue("#AEROS-1773")
    @Title("admin user can view a fiber group information")
    @WithTag(type = "feature", name = "adminUserFiberGroupViewInfoStory")
    public void can_view_a_fiber_group_information() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.createAProject(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());
        FiberGroup fiberGroup = fiberGroupSteps.getDefaultFiberGroupDataObject();
        fiberGroupSteps.verifyCreateFiberGroupDetails(fiberGroup, defaultFiberGroup);
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupAction(defaultFiberGroup.getFiberGroupName(), DETAILS_ACTION);
        fiberGroupSteps.isFormattedWindowTitleCorrect(FIBER_GROUP_NAME_WINDOW, defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.verifyViewFiberGroupInformationDetails(fiberGroup);
    }

    @Test
    @Issue("#AEROS-1784")
    @Title("admin user can create fiber group of all media types")
    @WithTag(type = "feature", name = "mediaTypesFiberGroupsCreationStory")
    public void can_create_all_media_type_fiber_groups() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        List<String> mediaTypes = fiberGroupSteps.getMediaTypeNames();
        fiberGroupSteps.verifyCreatedMediaTypes(mediaTypes);
    }

    @Test
    @Issue("#AEROS-1787")
    @Title("user can create multiple fiber groups per line")
    @WithTag(type = "feature", name = "multipleFiberGroupCreationStory")
    public void can_create_multiple_fiber_groups() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.isTooltipContentBlockPresent(false);
        fiberGroupSteps.clickOnTooltipButton();
        fiberGroupSteps.isTooltipContentBlockPresent(true);
        fiberGroupSteps.verifyTooltipTextCorrect(EXPAND_AREA);
        fiberGroupSteps.extendFiberGroupTextArea();
        fiberGroupSteps.enterFiberGroupPerLine(fiberGroupWithDigits.getFiberGroupName(),
                fiberGroupWithSymbols.getFiberGroupName(), fiberGroupWithSpaces.getFiberGroupName());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        messageConstraint = fiberGroupWithDigits.getFiberGroupName() + " " +
                fiberGroupWithSymbols.getFiberGroupName() + " " + fiberGroupWithSpaces.getFiberGroupName().trim();
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, messageConstraint);
        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithDigits);
        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithSymbols);
        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithSpaces);

    }

    @Test
    @Issue("#AEROS-1794")
    @Title("user can create multiple media type fiber groups per line")
    @WithTag(type = "feature", name = "multipleMediaTypeFiberGroupCreationStory")
    public void can_create_multiple_media_type_fiber_groups() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.isTooltipContentBlockPresent(false);
        fiberGroupSteps.clickOnTooltipButton();
        fiberGroupSteps.isTooltipContentBlockPresent(true);
        fiberGroupSteps.verifyTooltipTextCorrect(EXPAND_AREA);
        fiberGroupSteps.extendFiberGroupTextArea();
        fiberGroupSteps.enterFiberGroupPerLine(fiberGroupWithDigits.getFiberGroupName(),
                fiberGroupWithSymbols.getFiberGroupName(), fiberGroupWithSpaces.getFiberGroupName());
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        messageConstraint = fiberGroupWithDigits.getFiberGroupName() + " " +
                fiberGroupWithSymbols.getFiberGroupName() + " " + fiberGroupWithSpaces.getFiberGroupName().trim();

        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithDigits);
        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithSymbols);
        fiberGroupSteps.isFiberGroupPresent(fiberGroupWithSpaces);
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUP_ADDED, messageConstraint);
    }

    @Test
    @Issue("#AEROS-1795")
    @Title("admin user can delete fiber groups simultaneously")
    @WithTag(type = "feature", name = "adminUserFiberGroupsDeletingStory")
    public void can_delete_fiber_groups_at_the_same_time() throws InterruptedException {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);
        randomFiberGroup = FiberGroup.builder(RandomUtil.generateRandomFormat("fiber_group", FULL_FORMAT)).build();
        fiberGroupSteps.createAFiberGroup(randomFiberGroup.getFiberGroupName());
        fiberGroupSteps.isFiberGroupPresent(randomFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.selectFiberGroupByName(randomFiberGroup.getFiberGroupName());

        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.verifyDeletingFiberGroupNamesCorrect(selectedFiberGroupItems);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);
        commonSteps.verifySuccessfulNotificationMessage(FIBER_GROUPS_REMOVED);
        fiberGroupSteps.waitForFiberGroupToBeAbsent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupAbsent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupAbsent(randomFiberGroup);
    }

    @Test
    @Issue("#AEROS-1796")
    @Title("admin user can create fiber group with optional parameters")
    @WithTag(type = "feature", name = "fiberGroupOptionalParametersCreationStory")
    public void can_create_fiber_group_with_optional_parameters() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(fullDetailsFiberGroup.getFiberGroupName());
        fiberGroupSteps.enterFiberGroupComment(fullDetailsFiberGroup.getComment());
        fiberGroupSteps.selectFiberTypeOption(fullDetailsFiberGroup.getFiberType());
        fiberGroupSteps.setStartingFiberNumberValue(fullDetailsFiberGroup.getStartingFiberNumber());
        fiberGroupSteps.setNumberOfFibersValue(fullDetailsFiberGroup.getNumberOfFibers());
        fiberGroupSteps.setConnectionsValue(fullDetailsFiberGroup.getNumberOfConnections());
        fiberGroupSteps.setSplicesValue(fullDetailsFiberGroup.getNumberOfSplicers());

        fiberGroupSteps.setEnd1NameValue(fullDetailsFiberGroup.getEnd1Name());
        fiberGroupSteps.setEndConnectorValueByName("End1 Connector", fullDetailsFiberGroup.getEnd1Connector());
        fiberGroupSteps.selectEndConnectorCheckBox("End1 Connector", fullDetailsFiberGroup.getEnd1ConnectorOption());

        fiberGroupSteps.setEnd2NameValue(fullDetailsFiberGroup.getEnd2Name());
        fiberGroupSteps.setEndConnectorValueByName("End2 Connector", fullDetailsFiberGroup.getEnd2Connector());
        fiberGroupSteps.selectEndConnectorCheckBox("End2 Connector", fullDetailsFiberGroup.getEnd2ConnectorOption());
        FiberGroup fiberGroup = fiberGroupSteps.getFiberGroupDataObject();

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.selectFiberGroupAction(fullDetailsFiberGroup.getFiberGroupName(), DETAILS_ACTION);
        fiberGroupSteps.isFormattedWindowTitleCorrect(FIBER_GROUP_NAME_WINDOW, fullDetailsFiberGroup.getFiberGroupName());
        fiberGroupSteps.verifyViewModifiedFiberGroupInformationDetails(fiberGroup);

    }

    @Test
    @Issue("#AEROS-1829")
    @Title("admin user can create Media type fiber group with optional parameters")
    @WithTag(type = "feature", name = "mediaTypeFiberGroupOptionalParametersCreationStory")
    public void can_create_media_type_fiber_group_with_optional_parameters() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(fullDetailsFiberGroup.getFiberGroupName());
        fiberGroupSteps.enterFiberGroupComment(fullDetailsFiberGroup.getComment());
        fiberGroupSteps.selectFiberTypeOption(fullDetailsFiberGroup.getFiberType());
        fiberGroupSteps.setStartingFiberNumberValue(fullDetailsFiberGroup.getStartingFiberNumber());
        fiberGroupSteps.setNumberOfFibersValue(fullDetailsFiberGroup.getNumberOfFibers());
        fiberGroupSteps.setConnectionsValue(fullDetailsFiberGroup.getNumberOfConnections());
        fiberGroupSteps.setSplicesValue(fullDetailsFiberGroup.getNumberOfSplicers());

        fiberGroupSteps.setEnd1NameValue(fullDetailsFiberGroup.getEnd1Name());
        fiberGroupSteps.setEndConnectorValueByName("End1 Connector", fullDetailsFiberGroup.getEnd1Connector());
        fiberGroupSteps.selectEndConnectorCheckBox("End1 Connector", fullDetailsFiberGroup.getEnd1ConnectorOption());

        fiberGroupSteps.setEnd2NameValue(fullDetailsFiberGroup.getEnd2Name());
        fiberGroupSteps.setEndConnectorValueByName("End2 Connector", fullDetailsFiberGroup.getEnd2Connector());
        fiberGroupSteps.selectEndConnectorCheckBox("End2 Connector", fullDetailsFiberGroup.getEnd2ConnectorOption());
        FiberGroup fiberGroup = fiberGroupSteps.getFiberGroupDataObject();

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.selectFiberGroupAction(fullDetailsFiberGroup.getFiberGroupName(), DETAILS_ACTION);
        fiberGroupSteps.isFormattedWindowTitleCorrect(FIBER_GROUP_NAME_WINDOW, fullDetailsFiberGroup.getFiberGroupName());
        fiberGroupSteps.verifyViewModifiedFiberGroupInformationDetails(fiberGroup);

    }

    @Test
    @Issue("#AEROS-1830")
    @Title("admin user can create project collection")
    @WithTag(type = "feature", name = "projectCollectionCreationStory")
    public void can_create_project_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.clickOnProjectUploadButton();
        fiberGroupSteps.isModalWindowTitleCorrect(ADD_COLLECTION_WINDOW);
        fiberGroupSteps.isSaveButtonEnabled(false);
        fiberGroupSteps.enterCollectionName("test_collection");
        fiberGroupSteps.isSaveButtonEnabled(true);
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_CREATED);
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "test_collection");
        fiberGroupSteps.isProjectAttachmentLabelCorrect(ATTACHED_TO_A_PROJECT, project.getProjectName());
        fiberGroupSteps.isCloseButtonEnabled();
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.isAppliedProjectCollectionButtonPresent(true);
        fiberGroupSteps.isNumberOfProjectAttachedDocumentsCorrect(0);
    }

    @Test
    @Issue("#AEROS-1832")
    @Title("admin user can add collection to a fiber group")
    @WithTag(type = "feature", name = "addingFiberGroupCollectionStory")
    public void can_add_fiber_group_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.selectFiberGroupAction(defaultFiberGroup.getFiberGroupName(), UPLOAD_ACTION);
        fiberGroupSteps.isModalWindowTitleCorrect(ADD_COLLECTION_WINDOW);
        fiberGroupSteps.isSaveButtonEnabled(false);
        fiberGroupSteps.enterCollectionName("test_collection");
        fiberGroupSteps.isSaveButtonEnabled(true);
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_CREATED);
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "test_collection");
        fiberGroupSteps.isProjectAttachmentLabelCorrect(ATTACHED_TO_A_FIBER_GROUP, defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCloseButtonEnabled();
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.isFiberGroupCollectionButtonPresent(defaultFiberGroup, true);
        fiberGroupSteps.isNumberOfFiberGroupAttachedDocumentsCorrect(defaultFiberGroup, 0);
    }

    @Test
    @Issue("#AEROS-1837")
    @Title("admin user can delete project collection")
    @WithTag(type = "feature", name = "deletingProjectCollectionStory")
    public void can_delete_project_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.clickOnProjectUploadButton();
        fiberGroupSteps.enterCollectionName("test_collection");
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.clickOnRemoveProjectCollectionButton();
        fiberGroupSteps.isModalWindowTitleCorrect(DELETE_FILE_COLLECTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_DELETED);
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.isAppliedProjectCollectionButtonPresent(false);
    }

    @Test
    @Issue("#AEROS-1849")
    @Title("admin user can delete fiber group collection")
    @WithTag(type = "feature", name = "deletingFiberGroupCollectionStory")
    public void can_delete_fiber_group_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.selectFiberGroupAction(defaultFiberGroup.getFiberGroupName(), UPLOAD_ACTION);
        fiberGroupSteps.isModalWindowTitleCorrect(ADD_COLLECTION_WINDOW);
        fiberGroupSteps.enterCollectionName("test_collection");
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_CREATED);
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "test_collection");
        fiberGroupSteps.isProjectAttachmentLabelCorrect(ATTACHED_TO_A_FIBER_GROUP, defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.isCloseButtonEnabled();
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.isFiberGroupCollectionButtonPresent(defaultFiberGroup, true);
        fiberGroupSteps.clickOnDeleteFiberGroupCollectionButton(defaultFiberGroup);
        fiberGroupSteps.waitForModalWindowVisibility();
        fiberGroupSteps.isModalWindowTitleCorrect(DELETE_FILE_COLLECTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_DELETED);
        commonSteps.waitForSuccessfulNotificationMessageInvisibility();
        fiberGroupSteps.isFiberGroupCollectionButtonPresent(defaultFiberGroup, false);
    }

    @Test
    @Issue("#AEROS-1855")
    @Title("admin user can edit project collection")
    @WithTag(type = "feature", name = "editingProjectCollectionStory")
    public void can_edit_project_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createProjectCollection("test_project_collection");
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.isProjectCollectionNameCorrect("test_project_collection");
        fiberGroupSteps.isAppliedProjectCollectionButtonPresent(true);
        fiberGroupSteps.clickOnProjectCollectionButton();
        fiberGroupSteps.clickOnEditCollectionButton();
        fiberGroupSteps.enterCollectionName("edited_collection");
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_UPDATED);
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "edited_collection");
        fiberGroupSteps.isProjectCollectionNameCorrect("edited_collection");
    }

    @Test
    @Issue("#AEROS-1856")
    @Title("admin user can edit fiber group collection")
    @WithTag(type = "feature", name = "editingFiberGroupCollectionStory")
    public void can_edit_fiber_group_collection() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.createAFiberGroup(defaultFiberGroup.getFiberGroupName());
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.createFiberGroupCollection(defaultFiberGroup, "test_collection");
        fiberGroupSteps.closeCollectionWindow();
        fiberGroupSteps.isNumberOfFiberGroupAttachedDocumentsCorrect(defaultFiberGroup, 0);
        fiberGroupSteps.isFiberGroupCollectionNameCorrect(defaultFiberGroup, "test_collection");
        fiberGroupSteps.clickOnFiberGroupCollection(defaultFiberGroup);
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "test_collection");
        fiberGroupSteps.clickOnEditCollectionButton();
        fiberGroupSteps.enterCollectionName("edited_collection");
        fiberGroupSteps.clickOnAddCollectionSaveButton();
        commonSteps.verifySuccessfulNotificationMessage(FILE_COLLECTION_UPDATED);
        fiberGroupSteps.isFormattedWindowTitleCorrect(COLLECTION_WINDOW, "edited_collection");
        fiberGroupSteps.isFiberGroupCollectionNameCorrect(defaultFiberGroup, "edited_collection");
    }

    @Test
    @Issue("#AEROS-1874")
    @Title("verify add new fiber group window")
    @WithTag(type = "feature", name = "verifyAddNewFiberGroupWindowStory")
    public void verify_create_new_fiber_group_functionality() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);
        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setStartingFiberNumberValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setStartingFiberNumberValue("dsadsadas");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setStartingFiberNumberValue("10000");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setStartingFiberNumberValue("9999");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setNumberOfFibersValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setNumberOfFibersValue("dsadsadas");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setNumberOfFibersValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setConnectionsValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setConnectionsValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setConnectionsValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setConnectionsValue("dsadsadas");
        fiberGroupSteps.isConnectionsCorrect("");

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);

        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);

        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setConnectionsValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setSplicesValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setSplicesValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setSplicesValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setSplicesValue("dsadsadas");
        fiberGroupSteps.isSplicesValueCorrect("");

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);

        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);

        fiberGroupSteps.selectFiberGroupAction(ADD_ACTION);
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setSplicesValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());
    }

    @Test
    @Issue("#AEROS-1876")
    @Title("verify Media type Create new fiber group window")
    @WithTag(type = "feature", name = "verifyMediaTypeAddNewFiberGroupWindowStory")
    public void verify_media_type_create_new_fiber_group_functionality() {
        userSteps.openWebBasePage();
        userSteps.logsInToAflWeb(adminUser);
        projectSteps.clicksCreateNewProjectButton();
        projectSteps.isModalContentDisplayed();
        projectSteps.enterProjectName(project.getProjectName());
        projectSteps.clickOnProjectSubmitButton();
        projectSteps.waitForProject(project.getProjectName());
        projectSteps.verifyProjectNamePresence(project.getProjectName());
        projectSteps.goToProjectFiberGroupPage(project.getProjectName());
        fiberGroupSteps.waitForPageTitleDisappear(PROJECTS_TAB);

        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setStartingFiberNumberValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setStartingFiberNumberValue("dsadsadas");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setStartingFiberNumberValue("10000");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setStartingFiberNumberValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setStartingFiberNumberValue("9999");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setNumberOfFibersValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setNumberOfFibersValue("dsadsadas");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setNumberOfFibersValue("13.2");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.isModalContentBlockDisplayed();

        fiberGroupSteps.setNumberOfFibersValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setConnectionsValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setConnectionsValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setConnectionsValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setConnectionsValue("dsadsadas");
        fiberGroupSteps.isConnectionsCorrect("");

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);

        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);

        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setConnectionsValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setSplicesValue("0");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setSplicesValue("-1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(false);

        fiberGroupSteps.setSplicesValue(" 1");
        fiberGroupSteps.isCreateFiberGroupButtonActive(true);

        fiberGroupSteps.setSplicesValue("dsadsadas");
        fiberGroupSteps.isSplicesValueCorrect("");

        fiberGroupSteps.clickOnCreateFiberGroupButton();
        fiberGroupSteps.waitForFiberGroupToBePresent(defaultFiberGroup);
        fiberGroupSteps.isFiberGroupPresent(defaultFiberGroup);

        fiberGroupSteps.selectFiberGroupByName(defaultFiberGroup.getFiberGroupName());
        selectedFiberGroupItems = fiberGroupSteps.getSelectedFiberGroupNames();
        fiberGroupSteps.selectFiberGroupAction(DELETE_ACTION);
        fiberGroupSteps.clickOnModalWindowButtonByText(CONFIRM_DELETE_BUTTON);

        fiberGroupSteps.selectMediaTypeByName(defaultFiberGroup.getFiberType());
        fiberGroupSteps.enterFiberGroupName(defaultFiberGroup.getFiberGroupName());

        fiberGroupSteps.setSplicesValue("2147483648");
        fiberGroupSteps.clickOnCreateFiberGroupButton();
        commonSteps.verifyErrorNotificationMessage(FIBER_GROUP_ADDING_ERROR, defaultFiberGroup.getFiberGroupName());
    }

*/
  /*  
    @After
    public void tearDown() {
        driver.quit();
        restActions.deleteProjects();
    }
    
    */

}